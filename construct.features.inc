<?php
/**
 * @file
 * construct.features.inc
 */

/**
 * Implements hook_node_info().
 */
function construct_node_info() {
  $items = array(
    'dataset' => array(
      'name' => t('Dataset'),
      'base' => 'node_content',
      'description' => t('Creation of a new dataset for this conStruct node'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
