construct uses the structWSF (both part of the Open Semantic Framework) web services 
endpoints to operate. 

== Installation ==

You can install the full Open Semantic Framework stack by 
using this OSF installation script:

   http://techwiki.openstructs.org/index.php/Open_Semantic_Framework_Installation
                                                                                 

Additionally, you can read the full installation manual from the TechWiki:

  http://techwiki.openstructs.org/index.php/StructWSF_Installation_Guide


== PHP API Library ==

The construct module depends on the structWSF PHP API to perform all
communications with the configured structWSF instances.  To install this
library you will need to:

 * Enable the Libraries and XAutoload modules
 * Downloaded or clone from GitHub: https://github.com/structureddynamics/structWSF-PHP-API
 * Make sure the library is in sites/all/libraries/structWSF-PHP-API
