<?php


/*! @ingroup ConStructModule */
//@{

/*! @file Forms.php
   @brief Parse and manage forms file. Form files are are created from ontologies to create forms mainly used
          to create and update records descriptions of certain types.

   \n\n

  @author Frederick Giasson, Structured Dynamics LLC.

   \n\n\n
 */


/*!  @brief Parse a form file
     @details 

    \n

    @author Frederick Giasson, Structured Dynamics LLC.

    \n\n\n
*/
class Forms
{
  public $prefixes = array();
  public $forms = array();
  
  /*!   @brief Constructor

      \n

      @param[in] $formFile XML file describing the form to parse

      @author Frederick Giasson, Structured Dynamics LLC.

      \n\n\n
  */  
  function __construct($formFile)
  {
    $xml = new SimpleXMLElement($formFile);
    
    foreach($xml->xpath('//prefix') as $prefix) 
    {
      $this->prefixes[(string)$prefix["prefix"]] = (string)$prefix["uri"];
    }

    foreach($xml->xpath('//form') as $form) 
    {    
      $fields = array();
      
      foreach($form->xpath('./field') as $key => $field) 
      {
        $prefLabel = (string)$field->prefLabel;
        $description = (string)$field->description;
        $rel = (string)$field->rel;
        $name = preg_replace("/[^A-Za-z0-9]/", "_", $rel);
        
            //      ereg('/\.([^\.]*$)/', $this->file_src_name, $extension);
            //preg_match('/\.([^\.]*$)/', $this->file_src_name, $extension);

        // replace possible prefix.
        $pos = strpos($rel, ":", 0);
        if($pos !== FALSE)
        {
          $prefix = substr($rel, 0, $pos);
          $rel = str_replace($prefix.":", $this->prefixes[$prefix], $rel);
        }
        
        $control = "";
        if(isset($field["control"]))
        {
          $control = (string)$field["control"];
        }

        $maxValues = -1;
        if(isset($field->maxValues))
        {
          $maxValues = (int)$field->maxValues;
        }

        $minValues = -1;
        if(isset($field->minValues))
        {
          $minValues = (int)$field->minValues;
        } 
        
        $values = array();
        if(isset($field->values))
        {
		      foreach($field->values->value as $value)
		      {
		        array_push($values, (string)$value);
		      }
        }       
        
        array_push($fields, new Field($prefLabel, $description, $rel, $name, ($key + 1), $maxValues, $minValues, $control, $values));
      } 

      $type = (string)$form["type"];
      
      // replace possible prefix.
      $pos = strpos($type, ":", 0);
      if($pos !== FALSE)
      {
        $prefix = substr($type, 0, $pos);
        $type = str_replace($prefix.":", $this->prefixes[$prefix], $type);
      }
      
      array_push($this->forms, new Form($type, (string)$form->prefLabel, (string)$form->prefLabel, $fields));
    }
  }
  
  public function getFormByType($type)
  {
    // replace possible prefix.
    $pos = strpos($type, ":", 0);
    if($pos !== FALSE)
    {
      $prefix = substr($type, 0, $pos);
      
      if(isset($this->prefixes[$prefix]))
      {
        $type = str_replace($prefix.":", $this->prefixes[$prefix], $type);
      }
    }
    
    foreach($this->forms as $form)
    {
      if($form->type == $type)
      {
        return($form);
      }
    }
    
    return(NULL);
  }
}

class Form
{
  public $fields = array();
  public $type = "";
  public $prefLabel = "";
  public $description = "";
  
  /*!   @brief Constructor

      \n

      @param[in] 

      @author Frederick Giasson, Structured Dynamics LLC.

      \n\n\n
  */  
  function __construct($type, $prefLabel, $description, $fields)
  {
    $this->fields = $fields;
    $this->type = $type;
    $this->prefLabel = $prefLabel;
    $this->description = $description;
  }
}

class Field
{
  public $prefLabel = "";
  public $description = "";
  public $rel = "";
  /* @brief "name" in the sence of an html input element's name. */
  public $name = "";
  public $position = "";
  public $control = "text";
  public $maxValues = -1;
  public $minValues = -1;
  public $values = array();
    
  /*!   @brief Constructor

      \n

      @param[in] 

      @author Frederick Giasson, Structured Dynamics LLC.

      \n\n\n
  */  
  function __construct($prefLabel, $description, $rel, $name, $position, $maxValues=-1, $minValues=-1, $control="text", $values)
  {
    $this->prefLabel = $prefLabel;
    $this->description = $description;
    $this->rel = $rel;  
    $this->name = $name;
    $this->position = $position;
    $this->control = $control;
    $this->maxValues = $maxValues;
    $this->minValues = $minValues;
    $this->values = $values;
  }
}

//@}

?>