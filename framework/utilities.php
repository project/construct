<?php



/*! @ingroup ConStructModule */
//@{

/*! @file utilities.php
   @brief Define utilities functions.
   @details Utilities functions are function that can be used by any tool or script of the construct Drupal module.
   
   \n\n
 
   @author Frederick Giasson, Structured Dynamics LLC.

   \n\n\n
*/

use \StructuredDynamics\structwsf\framework\Namespaces;

/*!   @brief Get a prefix for a given ontology URI
     @details   This function return the prefix literal for a given ontologu URI. This is a simple procedure that associate an Ontology URI
                    to its "generally used prefix".

    \n
    
    @attention All the data ingested by a construct node has to define a prefix for the ontologies used to describe any data.
           
    @param[in] $uri is the URI of a class or a property of an ontology
           
    @return A string where the class or the property is prefixed with the most commonly used prefix
  
    
    @author Frederick Giasson, Structured Dynamics LLC.
  
    \n\n\n
*/
function get_uri_label($uri)
{
  // Find the base URI of the ontology
  $pos = strripos($uri, "#");

  if ($pos === FALSE)
  {
    $pos = strripos($uri, "/");
  }

  if ($pos !== FALSE)
  {
    $pos++;
  }

  // Save the URI of the ontology
  $onto = substr($uri, 0, $pos);

  // Save the URI of the class or property passed in parameter
  $resource = substr($uri, $pos, strlen($uri) - $pos);

  // This statement associate a base ontology URI to its prefix.
  switch ($onto)
  {
    case "http://xmlns.com/foaf/0.1/":
      return "foaf:" . $resource;
      break;

    case "http://blogs.yandex.ru/schema/foaf/":
      return "yandex_foaf:" . $resource;
      break;

    case "http://purl.org/rss/1.0/":
      return "rss:" . $resource;
      break;

    case "http://rdfs.org/sioc/ns#":
      return "sioc:" . $resource;
      break;

    case "http://usefulinc.com/ns/doap#":
      return "doap:" . $resource;
      break;

    case "http://www.w3.org/2003/01/geo/wgs84_pos#":
      return "geo:" . $resource;
      break;

    case "http://www.geonames.org/ontology#":
      return "geonames:" . $resource;
      break;

    case "http://creativecommons.org/ns#":
      return "cc:" . $resource;
      break;

    case "http://sw.cyc.com/2006/07/27/cyc/":

    case "http://sw.opencyc.org/2008/06/10/concept/en/":

    case "http://sw.opencyc.org/2008/06/10/concept/":

    case "http://sw.opencyc.org/concept/":

    case "http://sw.cyc.com/concept/":
      return "cyc:" . $resource;
      break;

    case "http://purl.org/umbel/sc/":

    case "http://purl.org/umbel/ac/":

    case "http://umbel.org/ns/":

    case "http://umbel.org/ns/sc/":

    case "http://umbel.org/ns/ac/":

    case "http://umbel.org/umbel/":

    case "http://umbel.org/umbel#":

    case "http://umbel.org/umbel/sc/":

    case "http://umbel.org/umbel/ac/":
      return "umbel:" . $resource;
      break;

    case "http://www.w3.org/2000/01/rdf-schema#":
      return "rdfs:" . $resource;
      break;

    case "http://www.w3.org/1999/02/22-rdf-syntax-ns#":
      return "rdf:" . $resource;
      break;

    case "http://www.w3.org/2002/07/owl#":
      return "owl:" . $resource;
      break;

    case "http://purl.org/dc/terms/":
      return "dcterms:" . $resource;
      break;

    case "http://www.w3.org/2004/02/skos/core#":

    case "http://www.w3.org/2008/05/skos#":
      return "skos:" . $resource;
      break;

    case "http://purl.org/ontology/po/":
      return "po:" . $resource;
      break;

    case "http://purl.org/NET/c4dm/event.owl#":
      return "event:" . $resource;
      break;

    case "http://purl.org/vocab/frbr/core#":
      return "frbr:" . $resource;
      break;

    case "http://purl.org/ontology/mo/":
      return "mo:" . $resource;
      break;

    case "http://purl.org/ontology/bibo/":
      return "bibo:" . $resource;
      break;

    case "http://purl.org/ontology/bkn#":
      return "bkn:" . $resource;
      break;

    case "http://purl.org/ontology/bkn/central#":
      return "bkn_central:" . $resource;
      break;

    case "http://purl.org/ontology/bkn/temp#":
      return "bkn-temp:" . $resource;
      break;

    case "http://purl.org/ontology/bkn/base/":
      return "bkn-base:" . $resource;
      break;

    case "http://purl.org/vocab/bio/0.1/":
      return "bio:" . $resource;
      break;

    case "http://schemas.talis.com/2005/address/schema#":
      return "address:" . $resource;
      break;

    case "http://schemas.talis.com/2005/address/schema#":
      return "address:" . $resource;
      break;

    case "http://purl.org/ontology/swt#":
      return "swt:" . $resource;
      break;

    case "http://purl.org/ontology/iron#":
      return "iron:" . $resource;
      break;

    case "http://purl.org/ontology/muni#":
      return "muni:" . $resource;
      break;

    case "http://purl.org/ontology/peg#":
      return "peg:" . $resource;
      break;

    case "http://purl.org/ontology/now#":
      return "now:" . $resource;
      break;

    case "http://www.geonames.org/ontology#":
      return "geoname:" . $resource;
      break;

    // By default, we return the "un-prefixed" URI passed in parameter.
    default:
      return $uri;
      break;
  }
}

/*!   @brief Get a full URI for a given prefixed class, property or individual
     @details   This function return the full URI for a given prefixed class, property or individual.

    \n
    
    @attention All the data ingested by a construct node has to define a prefix for the ontologies used to describe any data.
           
    @param[in] $label is the prefixed class, property or individual (example: foaf:Person)
           
    @return the full URI of the prefixed class, property or individual
    
    @author Frederick Giasson, Structured Dynamics LLC.
  
    \n\n\n
*/
function get_label_uri($label)
{
  $pos = strripos($label, ":");

  if ($pos === FALSE)
  {
    return ($label);
  }

  $pos++;

  $onto = substr($label, 0, $pos);

  $resource = substr($label, $pos, strlen($label) - $pos);

  switch ($onto)
  {
    case "foaf:":
      return "http://xmlns.com/foaf/0.1/" . $resource;
      break;

    case "yandex_foaf:":
      return "http://blogs.yandex.ru/schema/foaf/" . $resource;
      break;

    case "rss:":
      return "http://purl.org/rss/1.0/" . $resource;
      break;

    case "sioc:":
      return "http://rdfs.org/sioc/ns#" . $resource;
      break;

    case "doap:":
      return "http://usefulinc.com/ns/doap#" . $resource;
      break;

    case "geo:":
      return "http://www.w3.org/2003/01/geo/wgs84_pos#" . $resource;
      break;

    case "geonames:":
      return "http://www.geonames.org/ontology#" . $resource;
      break;

    case "cc:":
      return "http://creativecommons.org/ns#" . $resource;
      break;

    case "rdfs:":
      return "http://www.w3.org/2000/01/rdf-schema#" . $resource;
      break;

    case "rdf:":
      return "http://www.w3.org/1999/02/22-rdf-syntax-ns#" . $resource;
      break;

    case "owl:":
      return "http://www.w3.org/2002/07/owl#" . $resource;
      break;

    case "dcterms:":
      return "http://purl.org/dc/terms/" . $resource;
      break;

    case "skos:":
      return "http://www.w3.org/2008/05/skos#" . $resource;
      break;

    case "po:":
      return "http://purl.org/ontology/po/" . $resource;
      break;

    case "event:":
      return "http://purl.org/NET/c4dm/event.owl#" . $resource;
      break;

    case "frbr:":
      return "http://purl.org/vocab/frbr/core#" . $resource;
      break;

    case "mo:":
      return "http://purl.org/ontology/mo/" . $resource;
      break;

    case "bibo:":
      return "http://purl.org/ontology/bibo/" . $resource;
      break;

    case "bkn:":
      return "http://purl.org/ontology/bkn#" . $resource;
      break;

    case "bkn-temp:":
      return "http://purl.org/ontology/bkn/temp#" . $resource;
      break;

    case "bkn_central:":
      return "http://purl.org/ontology/bkn/central#" . $resource;
      break;

    case "bkn-base:":
      return "http://purl.org/ontology/bkn/base/" . $resource;
      break;

    case "bio:":
      return "http://purl.org/vocab/bio/0.1/" . $resource;
      break;

    case "address:":
      return "http://schemas.talis.com/2005/address/schema#" . $resource;
      break;

    case "swt:":
      return "http://purl.org/ontology/swt#" . $resource;
      break;

    case "iron:":
      return "http://purl.org/ontology/iron#" . $resource;
      break;
      
    case "peg:":
      return "http://purl.org/ontology/peg#" . $resource;
      break;

    case "muni:":
      return "http://purl.org/ontology/muni#" . $resource;
      break;

    case "geoname:":
      return "http://www.geonames.org/ontology#" . $resource;
      break;      

    case "umbel:":
      return "http://umbel.org/umbel#" . $resource;
      break;      

    default:
      return $label;
      break;
  }
}

/*!   @brief Get the domain of a URL

    \n
    
    @param[in] $url the full URL
           
    @return the domain name of the URL *with* the prefix "http://"

    @author Frederick Giasson, Structured Dynamics LLC.
  
    \n\n\n
*/
function get_domain($url)
{
  if (strlen($url) > 8)
  {
    $pos = strpos($url, "/", 8);

    if ($pos === FALSE)
    {
      return $url;
    }
    else
    {
      return substr($url, 0, $pos);
    }
  }
  else
  {
    return $url;
  }
}

/*!   @brief Get the RDF serialization used for a given RDF document

    \n
    
    @param[in] $data a serialized RDF document
           
    @return   FALSE if the serialization format is unknown. Return one of these values otherwise:
            @li "application/rdf+xml"; if the RDF document is serialized using XML
            @li "application/rdf+n3"; if the RDF document is serialized using N3
    
    @author Frederick Giasson, Structured Dynamics LLC.
  
    \n\n\n
*/
function rdfDocumentSerialization(&$data)
{
  // Check if we have a RDF/XML document
  if (preg_match('/<.*:rdf>/i', $data))
  {
    return "application/rdf+xml";
  }

  // Check if we have a RDF/N3 document
  if (preg_match('/prefix:.*|<.*>.*[\.;]{1}|:.*[\.;]{1}/i', $data))
  {
    return "application/rdf+n3";
  }

  // Return FALSE if the serialization is unknown
  return FALSE;
}

/*!  @brief Convert the URI of an indexed resource to the URI schema used to browse the website.
        @details Sometimes, different URI(s) schemas are used to access a website. Sometimes one can use "www", other times it can use the Amazon public DNS, etc. We have to make sure that the user "sees" the same URIs schema along the way. So, if the browser tool return list of URIs of items to browse, we have to convert them to the schema used by the user (and not the schema used to index the resources references).

    @param[in] $uri URI to be converted  

    \n
    
    @return the converted URI to the current URI schema.
    
    @author Frederick Giasson, Structured Dynamics LLC.
  
    \n\n\n
*/
function convertResourceCurrentReference($uri)
{
  global $base_url;

  return (str_replace(get_domain($uri), get_domain($base_url), $uri));
}

/*!  @brief Get a Dataset URI from a resource URI
        @details This function is useful to get the uri of a dataset where a resource is archived from the URI of 
                     that resource. This is mainly used by the "read"/"dereferencing" functions of the system. This is 
                     possible because of the URI schema used by the system to build-up resources URI.

    @param[in] $uri URi of the resource from which we generate the URI of its dataset  

    \n
    
    @return URI of the dataset
    
    @author Frederick Giasson, Structured Dynamics LLC.
  
    \n\n\n
*/
function getDatasetFromUri($uri)
{
  // Try to get the dataset's URI from the legacy URI assignation.
  global $base_url;

  $pos = strpos($uri, "/wsf/datasets/");

  if ($pos !== FALSE)
  {
    $posEnd = strpos($uri, "/", ($pos + 14));
    
    // Nomalization of the URL of the node. Here we remove the "www" to normalize it.
    $normalized_base_url = str_replace("www.", "", $base_url);

    $domain = variable_get('construct_UrisDomain', str_replace("http://", "", get_domain($normalized_base_url)));

    return ("http://" . $domain . "/wsf/datasets/" . substr($uri, ($pos + 14), ($posEnd - ($pos + 14))) . "/");
  }
  else
  {
    // Try to get the URI of the dataset by querying the Drupal variables related to datasets
    // registration by checking with a part of the URI of the record.
     $results = db_select('construct_datasets', 'cd')
                 ->fields('cd')
                 ->condition('did', $uri, '=')
                 ->execute();

     foreach($results as $result)
     {
       return($result->nid);
     } 
  }

  return (FALSE);
}

/*!  @brief Get a OG group ID from a dataset URI generated by construct on a Drupal node.

    @param[in] $dataset URI of the dataset from which we find the Group ID.
    @param[in] $wsf Base URL of the structWSF network. If this parameter is not specified,
                    the first dataset will be returned, from any registered WSF network<
                    in the node.

    \n
    
    @return ID of the group. FALSE in ID can't be found
    
    @author Frederick Giasson, Structured Dynamics LLC.
  
    \n\n\n
*/
function getInternalIdFromDatasetUri($dataset, $wsf = "")
{
  // Try to get the group ID from the legacy URI assignation.  
  $pos = strpos($dataset, "/wsf/datasets/");

  if ($pos !== FALSE)
  {
    $posEnd = strpos($dataset, "/", ($pos + 14));

    if ($posEnd !== FALSE)
    {
      return (substr($dataset, ($pos + 14), ($posEnd - ($pos + 14))));
    }
  }
  else
  {
    // Try to get the URI of the dataset by querying the Drupal variables related to datasets
    // registration by checking with a part of the URI of the record.
     $results = db_select('construct_datasets', 'cd')
                 ->fields('cd')
                 ->condition('did', $dataset, '=')
                 ->execute();

     foreach($results as $result)
     {
       if($wsf != "")
       {
         if($wsf == $result->wsf)
         {
          return($result->nid);
         }
       }
       else
       {
         return($result->nid); 
       }
     }
  } 

  return (FALSE);
}

/*!  @brief Get a human readable label for a target resource

    @param[in] $subjectTriples Triples describing the resource for which we need a human readable label
    @param[in] $defaultLabel Default label to return if no label is fond

    \n
    
    @return Human readable label for that resource
    
    @author Frederick Giasson, Structured Dynamics LLC.
  
    \n\n\n
*/
function getResourceLabel($subjectTriples, $defaultLabel)
{
  $labelProperties = Namespaces::getLabelProperties();

  $labelCompositeProperties = array(
    Namespaces::$foaf . "givenname" => array(
      Namespaces::$foaf . "givenname",
      Namespaces::$foaf . "family_name"
    ),
    Namespaces::$foaf . "family_name" => array(
      Namespaces::$foaf . "givenname",
      Namespaces::$foaf . "family_name"
    )
  );

  foreach ($labelProperties as $property)
  {
    if (isset($subjectTriples[$property]))
    {
      // Check if it is part of a composite label
      if (isset($labelCompositeProperties[$property]))
      {
        $label = "";

        foreach ($labelCompositeProperties[$property] as $clabel)
        {
          $label .= $subjectTriples[$clabel][0]["text"] . " ";
        }

        return (substr($label, 0, (strlen($label) - 1)));
      }

      return $subjectTriples[$property][0]["text"];
    }
  }

  return ($defaultLabel);
}

/*
    Working with XML. Usage: 
    $xml=xml2ary(file_get_contents('1.xml'));
    $link=&$xml['ddd']['_c'];
    $link['twomore']=$link['onemore'];
    // ins2ary(); // dot not insert a link, and arrays with links inside!
    echo ary2xml($xml);
    
    from: http://mysrc.blogspot.com/2007/02/php-xml-to-array-and-backwards.html
*/

// XML to Array
function xml2ary(&$string)
{
  $parser = xml_parser_create();
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parse_into_struct($parser, $string, $vals, $index);
  xml_parser_free($parser);

  $mnary = array();
  $ary = &$mnary;

  foreach ($vals as $r)
  {
    $t = $r['tag'];

    if ($r['type'] == 'open')
    {
      if (isset($ary[$t]))
      {
        if (isset($ary[$t][0])) $ary[$t][] = array();
        else $ary[$t] = array(
          $ary[$t],
          array()
        );

        $cv = &$ary[$t][count($ary[$t]) - 1];
      }
      else $cv = &$ary[$t];

      if (isset($r['attributes']))
      {
        foreach ($r['attributes'] as $k => $v)
            $cv['_a'][$k] = $v;
      }
      $cv['_c'] = array();
      $cv['_c']['_p'] = &$ary;
      $ary = &$cv['_c'];
    }
    elseif ($r['type'] == 'complete')
    {
      if (isset($ary[$t]))
      { // same as open
        if (isset($ary[$t][0])) $ary[$t][] = array();
        else $ary[$t] = array(
          $ary[$t],
          array()
        );

        $cv = &$ary[$t][count($ary[$t]) - 1];
      }
      else $cv = &$ary[$t];

      if (isset($r['attributes']))
      {
        foreach ($r['attributes'] as $k => $v)
            $cv['_a'][$k] = $v;
      }
      $cv['_v'] = (isset($r['value']) ? $r['value'] : '');
    }
    elseif ($r['type'] == 'close')
    {
      $ary = &$ary['_p'];
    }
  }

  _del_p($mnary);
  return $mnary;
}

// _Internal: Remove recursion in result array
function _del_p(&$ary)
{
  foreach ($ary as $k => $v)
  {
    if ($k === '_p') unset($ary[$k]);

    elseif (is_array($ary[$k])) _del_p($ary[$k]);
  }
}

// Array to XML
function ary2xml($cary, $d = 0, $forcetag = '')
{
  $res = array();

  foreach ($cary as $tag => $r)
  {
    if (isset($r[0]))
    {
      $res[] = ary2xml($r, $d, $tag);
    }
    else
    {
      if ($forcetag) $tag = $forcetag;
      $sp = str_repeat("\t", $d);
      $res[] = "$sp<$tag";

      if (isset($r['_a']))
      {
        foreach ($r['_a'] as $at => $av)
            $res[] = " $at=\"$av\"";
      }
      $res[] = ">" . ((isset($r['_c'])) ? "\n" : '');

      if (isset($r['_c'])) $res[] = ary2xml($r['_c'], $d + 1);

      elseif (isset($r['_v'])) $res[] = $r['_v'];
      $res[] = (isset($r['_c']) ? $sp : '') . "</$tag>\n";
    }
  }
  return implode('', $res);
}

// Insert element into array
function ins2ary(&$ary, $element, $pos)
{
  $ar1 = array_slice($ary, 0, $pos);
  $ar1[] = $element;
  $ary = array_merge($ar1, array_slice($ary, $pos));
}

/**
* Get all the variables that match a variable name pattern. 
* 
* @param mixed $val Variable name search pattern that may includes the '%' wildcard character
*/
function variable_name_search($val)
{
  // @TODO: Rewrite this query to use placeholders to prevent SQL injection.
  //$resultset = db_query('SELECT name, value FROM {variable} WHERE `name` LIKE \''.$val.'\'');
  $resultset = db_query('SELECT name, value FROM {variable} WHERE name LIKE :val', array(':val' => $val));

  $results = array();
  
  while($values = $resultset->fetchAssoc())
  {
    $results[$values["name"]] = unserialize($values["value"]);
  }  
  
  return($results);
}

/**
* Generate a label from the ending of a URI.
* 
* @param mixed $uri
*/
function get_label_from_uri($uri)
{
  $label = '';

  // Get labels          
  $entity = entity_load('resource_type', array($uri));
  
  $getLabelFromUriEnding = FALSE;
  
  if(isset($entity[$uri]))
  {
    if(isset($entity[$uri]->preflabel))
    {
      $label = $entity[$uri]->preflabel['und'][0]['value'];
    }
    else
    {
      $getLabelFromUriEnding = TRUE;
    }
  }
  else
  {
    $getLabelFromUriEnding = TRUE;
  }    
  
  if($getLabelFromUriEnding)
  {
    // Find the base URI of the ontology
    $pos = strripos($uri, "#");

    if ($pos === FALSE)
    {
      $pos = strripos($uri, "/");
    }

    if ($pos !== FALSE)
    {
      $pos++;
    }

    // Save the URI of the class or property passed in parameter
    $label = substr($uri, $pos);
  }
  
  return($label);
}


//@}
