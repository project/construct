<?php


/*! @ingroup ConStructModule */
//@{

use \StructuredDynamics\structwsf\framework\Namespaces;

/*! @file Resource.php
   @brief Interact with the triple store to get information about a specific resource
  
   \n\n
 
   @author Frederick Giasson, Structured Dynamics LLC.

   \n\n\n
 */

/*!   @brief Interact with the triple store to get information about a specific resource
            
    \n
    
    @author Frederick Giasson, Structured Dynamics LLC.
  
    \n\n\n
*/
class Resource
{
  /*! @brief Array of triples where the current resource is a subject. */
  public $subjectTriples = array();

  /*! @brief Array of triples where the current resource is an object. */
  public $objectTriples = array();

  /*! @brief URI of the current resource */
  private $uri = "";

  /*! @brief URI of the dataset where the resource is indexed */
  private $dataset = "";

  /*! @brief Array of triples that reify triples of a resource description. */
  public $reificationTriples = array();

  /*! @brief Raw data (text/xml) describing the resource. This is normally used to feed another 
             module/template with the raw description of the resource. */
  public $rawSerialization = array();

  public $labelProperties = array();  
  
/*!   @brief Constructor
            
    \n
    
    @param[in] $uri URI of the resource being defined
    @param[in] $dataset URI of the dataset where to get the description of the resource. This parameter can be an empty string. If it is, the system will try to find the dataset URI from the resource URI.
    @param[in] $includeLinkbacks Tells the system if we include the object-triples (queries take longer to resolve)
    @param[in] $includeAttributesList Only get the attributes that are in this array in the resource's resultset
    
    @author Frederick Giasson, Structured Dynamics LLC.
  
    \n\n\n
*/
  function __construct($uri, $dataset, $includeLinkbacks = FALSE, $includeAttributesList = array())
  {
    include_once('./' . drupal_get_path('module', 'construct') . '/framework/ProcessorXML.php');

    $this->labelProperties = Namespaces::getLabelProperties();      
    
    $this->uri = $uri;

    $datasetId = 0;

    if ($dataset == "")
    {
      // Try to get a dataset reference from the URI of the resource
      $this->dataset = getDatasetFromUri($this->uri);
    }
    else
    {
      $this->dataset = $dataset;
    }

    // Check if the dataset is a linked dataset. If it is, then we get the ID of the local dataset that links to it;
    // otherwise we get the ID of the requested dataset (which is local)

    $linkedDatasetRegistry = variable_get("Linked-Dataset-Registry", array());

    $linkedId = 0;

    $key = array_search($this->dataset, $linkedDatasetRegistry);

    if ($key !== FALSE)
    {
      $linkedId = $key;
    }

    if ($linkedId == 0)
    {
      $datasetId = "";
      
      $datasetId = getInternalIdFromDatasetUri($this->dataset);
    }
    else
    {
      $datasetId = $linkedId;
    }

    global $base_url;

    // Archiving suject triples
    global $user;

    // Get Group ID from Dataset URI
    $wsfAddress = _construct_get_wsf($datasetId);

    if ($wsfAddress != "")
    {
      $userIP = variable_get("construct_AccessUser" . $user->uid, "");

      if ($userIP == "")
      {
        // If there is no IP for that user, we overload the IP of the node with "::"
        $userIP = "self::$user->uid";
      }

      // Get accessible datasets for this user

      // If we are in face of a linked dataset, we have to use the linked dataset URI as the target URI 
      // for this WSF query.
      $linkedDatasetRegistry = variable_get("Linked-Dataset-Registry", "");

      $targetDataset = $this->dataset;

      if ($linkedDatasetRegistry != "")
      {
        if (isset($linkedDatasetRegistry[getInternalIdFromDatasetUri($this->dataset)]))
        {
          $targetDataset = $linkedDatasetRegistry[getInternalIdFromDatasetUri($this->dataset)];
        }
      }

      $this->dataset = $targetDataset;

      $ialParam = "";
      if(count($includeAttributesList) > 0)
      {
        $ialParam = "&include_attributes_list=";
        
        foreach($includeAttributesList as $ial)
        {
          $ialParam .= urlencode($ial).";";
        }
        
        $ialParam = trim($ialParam, ";");
      }
      
      $wsq = new WebServiceQuerier($wsfAddress . "crud/read/", "get", "text/xml",
        "uri=" . urlencode($uri) . ($targetDataset != "" ? "&dataset=" . urlencode($targetDataset) : "") . "&registered_ip=" . urlencode($userIP)
        . "&include_reification=true&include_linksback=" . ($includeLinkbacks === TRUE ? "true" : "false"). $ialParam, 
        2);

      if ($wsq->getStatus() != 200)
      {
        if ($wsq->getStatus() == 400 && $wsq->error->id == "WS-CRUD-READ-300")
        {
          return;
        }
        else
        {
          $wsq->displayError();
          return ("");
        }

        return;
      }
      else
      {
        $xml = new ProcessorXML();
        $this->rawSerialization = $wsq->getResultset();
        $xml->loadXML($this->rawSerialization);

        $subjects = $xml->getSubjects();

        foreach ($subjects as $subject)
        {
          $subjectUri = $xml->getURI($subject);
          $subjectType = $xml->getType($subject, FALSE);

          if ($subjectUri != $uri && $includeLinkbacks === TRUE)
          {
            // Archiving object triples
            $predicates = $xml->getPredicates($subject);

            $predicateType = $xml->getType($predicates->item(0), FALSE);

            if (!isset($this->objectTriples[$predicateType]))
            {
              $this->objectTriples[$predicateType] = array();
            }

            $objects = $xml->getObjects($predicates->item(0));

            array_push($this->objectTriples[$predicateType], $subjectUri);
          }
          elseif ($subjectUri == $uri)
          {
            $predicates = $xml->getPredicates($subject);

            if (!isset($this->subjectTriples["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"]))
            {
              $this->subjectTriples["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"] = array();
            }

            array_push($this->subjectTriples["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"], array(
              $subjectType,
              ""
            ));

            foreach ($predicates as $predicate)
            {
              $predicateType = $xml->getType($predicate, FALSE);

              $objects = $xml->getObjects($predicate);
              $objectType = $xml->getType($objects->item(0));

              if (!isset($this->subjectTriples[$predicateType]))
              {
                $this->subjectTriples[$predicateType] = array();
              }

              if ($objectType == "rdfs:Literal")
              {
                array_push($this->subjectTriples[$predicateType], array(
                  $xml->getContent($objects->item(0)),
                  "http://www.w3.org/2001/XMLSchema#string"
                ));
              }
              else
              {
                array_push($this->subjectTriples[$predicateType], array(
                  $xml->getUri($objects->item(0)),
                  ""
                ));
              }

              // Check for possible reification statements
              $reifies = $xml->getReificationStatements($objects->item(0));

              foreach ($reifies as $reify)
              {
                // If the object of the reification is a literal
                if ($objectType == "rdfs:Literal")
                {
                  if (!isset(
                    $this->reificationTriples[$predicateType][$xml->getContent($objects->item(0))][$xml->getType($reify,
                      FALSE)]))
                  {
                    $this->reificationTriples[$predicateType][$xml->getContent($objects->item(0))][$xml->getType($reify,
                      FALSE)] = array();
                  }

                  array_push(
                    $this->reificationTriples[$predicateType][$xml->getContent($objects->item(0))][$xml->getType($reify,
                      FALSE)], $xml->getValue($reify));
                }
                // If the object of the reification is a resource
                else
                {
                  if (!isset(
                    $this->reificationTriples[$predicateType][$xml->getURI($objects->item(0))][$xml->getType($reify,
                      FALSE)]))
                  {
                    $this->reificationTriples[$predicateType][$xml->getURI($objects->item(0))][$xml->getType($reify,
                      FALSE)] = array();
                  }

                  array_push(
                    $this->reificationTriples[$predicateType][$xml->getURI($objects->item(0))][$xml->getType($reify,
                      FALSE)], $xml->getValue($reify));
                }
              }
            }
          }
        }

        unset($wsq);
      }
    }
    else
    {
      drupal_set_message("No WSF address linked to this dataset", "error", TRUE);
    }
  }

  function __destruct(){}

  /*!   @brief Get the types describing this resource
              
      \n
      
      @return returns an array of URIs of types for this resource. An empty array of no types exists
              for this resource.
    
      @author Frederick Giasson, Structured Dynamics LLC.
    
      \n\n\n
  */
  public function getTypes()
  {
    if(isset($this->subjectTriples["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"]) &&
       count($this->subjectTriples["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"]) > 0)
    {
      return ($this->subjectTriples["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"]);
    }
    else
    {
      return(array());
    }
  }

  /*!   @brief Get the dataset where the resource has been indexed
              
      \n
      
      @return returns the URI of the dataset where the resource has been indexed
    
      @author Frederick Giasson, Structured Dynamics LLC.
    
      \n\n\n
  */
  public function getDataset()
  {
    return ($this->dataset);
  }

  /*!   @brief Get the properties where the resource is a subject
              
      \n
      
      @return returns an array properties
    
      @author Frederick Giasson, Structured Dynamics LLC.
    
      \n\n\n
  */
  public function getSubjectProperties()
  {
    $properties = array();

    foreach ($this->subjectTriples as $property => $values)
    {
      array_push($properties, $property);
    }

    return $properties;
  }

  /*!   @brief Get the properties where the resource is a object
              
      \n
      
      @return returns an array properties
    
      @author Frederick Giasson, Structured Dynamics LLC.
    
      \n\n\n
  */
  public function getObjectProperties()
  {
    $properties = array();

    foreach ($this->objectTriples as $property => $values)
    {
      array_push($properties, $property);
    }

    return $properties;
  }

  /*!   @brief Get the values for a given property describing the resource
              
      \n
      
      @return returns an array of values for a given property 
    
      @author Frederick Giasson, Structured Dynamics LLC.
    
      \n\n\n
  */
  public function getPropertyValues($property)
  {
    if (isset($this->subjectTriples[$property]))
    {
      return ($this->subjectTriples[$property]);
    }

    if (isset($this->objectTriples[$property]))
    {
      return ($this->objectTriples[$property]);
    }

    return (array());
  }

  /*!   @brief Get a label for that resource
              
      \n
      
      @return returns Returns a literal that can be used as a label for that resource
    
      @author Frederick Giasson, Structured Dynamics LLC.
    
      \n\n\n
  */
  public function getLabel()
  {    
    foreach ($this->labelProperties as $property)
    {
      if (isset($this->subjectTriples[$property]))
      {
        return $this->subjectTriples[$property][0];
      }
    }

    // By default, the URI is returned as label.
    if (strlen($this->uri) > 40)
    {
      return array(
        substr($this->uri, 0, 40) . "...",
        ""
      );
    }

    return array(
      $this->uri,
      ""
    );
  }

  /*!   @brief Get labels for that resource
              
      \n
      
      @return returns Returns an array of literals that can be used as a label for that resource
    
      @author Frederick Giasson, Structured Dynamics LLC.
    
      \n\n\n
  */
  public function getLabels()
  {
    $labels = array();

    foreach ($this->labelProperties as $property)
    {
      if (isset($this->subjectTriples[$property]))
      {
        array_push($labels, $this->subjectTriples[$property][0]);
      }
    }

    return ($labels);
  }

  public function getReificationStatements()
  {
    return $this->reificationTriples;
  }
  
  /*!   @brief Raw data (text/xml) describing the resource. This is normally used to feed another 
               module/template with the raw description of the resource.
              
      \n
      
      @return returns Returns a string with the raw text/xml description data of the resource.
    
      @author Frederick Giasson, Structured Dynamics LLC.
    
      \n\n\n
  */  
  public function getRawSerialization()
  {
    return $this->rawSerialization;
  }
  
  public function getURI()
  {
    return($this->uri);
  }
  
}



//@}

?>


