
The user has to install the Smarty templating system in order to have a working construct module.

Smarty:
-----

You have to download the Smarty library from here: http://www.smarty.net/download.php

Unarchive this file in the "/construct/framework/smarty/" folder.