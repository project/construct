<?php

use \StructuredDynamics\structwsf\php\api\ws\auth\lister\AuthListerQuery;
use \StructuredDynamics\structwsf\php\api\ws\dataset\read\DatasetReadQuery;


function construct_linked_datasets_settings($form, &$form_state, $key = '') {
  $form['#description'] =  '<p>' . t('Choose the existing datasets to register for use with search and other conStruct functionality.') . '</p>';

  $form['construct_linked_datasets'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Linked Datasets'),
    '#options' => _construct_get_existing_datasets(),
    "#default_value" => variable_get('construct_linked_datasets', array()),
  );

  return system_settings_form($form);
}


function _construct_get_existing_datasets() {
  $ontologies = array();
  $datasets = array();
  $network = variable_get('construct_OntologySettings_network', 'http://localhost/ws/');

  // Create the AuthListerQuery object
  $authlister = new AuthListerQuery($network);

  // Specifies that we want to get all the dataset URIs available to the server that send this query.
  $authlister->getDatasetsUri();

  // Send the auth lister query to the endpoint
  $authlister->send(new DrupalQuerierExtension());

  if(!$authlister->isSuccessful()) {
    drupal_set_message(
      t('AuthListerQuery query error [%status] %msg (%msg_desc).',
      array(
        '%status' => $authlister->getStatus(),
        '%msg' => $authlister->getStatusMessage(),
        '%msg_desc' => $authlister->getStatusMessageDescription()
      )),
      'error'
    );
    return;
  }

  // Get back the resultset returned by the endpoint
  $resultset = $authlister->getResultset()->getResultset();

  foreach($resultset['unspecified'] as $result_node => $results) { // Use 'unspecified' because this list is always generated on the fly
    foreach ($results as $result_key => $result) {
      if ($result_key == 'type') {
        continue;
      }
      else {
        foreach ($result as $result_item) {
          if(structontology_is_ontology_dataset($result_item['uri'])) {
            $ontologies[] = $result_item['uri'];
          }
          else {
            $prefLabel = _construct_get_dataset_label($result_item['uri']);

            // Only return the datasets that have a proper prefLabel
            if ($prefLabel) $datasets[$result_item['uri']] = $prefLabel;
          }
        }
      }
    }
  }

  return $datasets;
}

function _construct_get_dataset_label($uri) {
  $network = variable_get('construct_OntologySettings_network', 'http://localhost/ws/');

  $datasetRead = new DatasetReadQuery($network);
  $datasetRead->uri($uri);
  $datasetRead->send();

  $prefLabel = $uri;
  if ($datasetRead->isSuccessful()) {
    $datasetRead_results = $datasetRead->getResultset()->getResultset();
    $prefLabel = (empty($datasetRead_results['unspecified'][$uri]['prefLabel']))
      ? $prefLabel
      : $datasets[$uri] = $datasetRead_results['unspecified'][$uri]['prefLabel'];
  }

  return $prefLabel;
}
