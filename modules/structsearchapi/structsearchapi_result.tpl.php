<?php
/**
 * @file: searchresultapi_result.tpl.php
 *
 * Variables
 * 
 * - $url - URL to the resource_type entity page view
 * - $name - The name of the resource_type entity
 * - $title - The marked up version of $name, which links to $url
 * - $entity - A ResourceType Entity class instance
 */
?>

<h3><?php print "<a href=\"/resources/".urlencode(urlencode($entity->uri))."\">".$entity->label."</a>"; ?></h3>

<div><?php print (isset($entity->description['und'][0]['value']) ? $entity->description['und'][0]['value'] : ''); ?></div>

