<?php
// Include struct code
use \StructuredDynamics\structwsf\php\api\ws\search\SearchQuery;
use \StructuredDynamics\structwsf\framework\Namespaces;
use \StructuredDynamics\structwsf\framework\Resultset;
use \StructuredDynamics\structwsf\framework\Subject;
use \StructuredDynamics\structwsf\php\api\ws\dataset\read\DatasetReadQuery;
use \StructuredDynamics\structwsf\php\api\ws\ontology\read\GetPropertyFunction;
use \StructuredDynamics\structwsf\php\api\ws\ontology\read\OntologyReadQuery;
use \StructuredDynamics\structwsf\php\api\ws\ontology\read\GetLoadedOntologiesFunction;
use \StructuredDynamics\structwsf\php\api\ws\crud\read\CrudReadQuery;

define('STRUCTSEARCHAPIINSERT', 'STRUCTSEARCHAPIINSERT');
define('STRUCTSEARCHAPIREMOVE', 'STRUCTSEARCHAPIREMOVE');
/**
 * Search service class that uses the structWSF service to retrieve data from.
 */
class SearchApiStructIndexService extends SearchApiAbstractService {
  protected $cacheURIs = array();
  protected $createFieldCBs = array(
    'text' => 'structsearchapi_createfield_string',
    'string' => 'structsearchapi_createfield_string',
    'list<string>' => 'structsearchapi_createfield_string_list',
    'property' => 'structsearchapi_createfield_property',
  );

  // Allow user to select which of the Networks to use for this search.
  public function configurationForm(array $form, array &$form_state) {
   
    // TODO change to select field 
    $form['dataset'] = array(
      '#type' => 'textfield',
      '#title' => 'Default Dataset',
      '#description' => 'This dataset will be used if an item does not define there own dataset',
      '#default_value' => isset($this->options['dataset']) ? $this->options['dataset']: "",
    );
    $form['debug'] = array(
      '#type' => 'checkbox',
      '#title' => 'Enable Debug',
      '#description' => 'use dpm to show debug information',
      '#default_value' => isset($this->options['debug']) ? $this->options['debug']: 0,
      '#options' => array( "No Debug", "Show Debug Infomation"),
    );
    return $form;
  }
  protected function debug($data, $title) {
    if($this->options['debug']) {
      if(function_exists('dpm') && FALSE) {
        dpm($data, $title);
      }
      else {
        drupal_set_message($title ."<br/>" . print_r($data, TRUE));
      }
    }
  }
 
  /**
   * This will look up the mapping from the index id to the uri used for deletes 
   * and saves
   *
   * It cache it locally about doing the query so that we do not have to query the 
   * db over and over
   *
   * @PARAM $index : a SearchApiIndex object 
   * @PARAM $reset : if true reload the cache from the db
   *
   * @RETURN: an array keye dby the $index_id for db object with item_id, index_id, and uri.
   */
  protected function cacheURIs(SearchApiIndex $index, $reset=FALSE) {
    if($reset || !isset($this->cacheURIs[$index->id])) {
      $query = db_select("structsearchapi_uri", "u");
      $query->fields("u", array("item_id", "uri"));
      $query->condition("u.index_id", $index->id);
      $this->cacheURIs[$index->id] = $query->execute()->fetchAllAssoc('item_id');
    }
    return $this->cacheURIs[$index->id];
  }

  /**
   * retrive a stored uri, based on an index and and $item_id
   *
   * @PARAM $item_id : an int that represents the id of the item in the $index
   * @PARAM $index : a SearchApiIndex object 
   *
   * @RETURN: a uri for the item or FALSE
   */
  protected function getURI($item_id, $index) {
    $uris = $this->cacheURIs($index);
    if(isset($uris[$item_id])) {
      return $uris[$item_id]->uri;
    }
  }

  /**
   * set a uri for storage
   *
   * @PARAM $item_id : an int that represents the id of the item in the $index
   * @PAAM $uri: the uri associated with the item
   * @PARAM $index : a SearchApiIndex object 
   *
   * @RETURN : self
   */
  protected function setURI($item_id, $uri, $index) {
    $record = new stdClass();
    $record->uri = $uri;
    $record->item_id = $item_id;
    $record->index_id = $index->id;
    drupal_write_record("structsearchapi_uri", $record);
    $this->cacheURIs($index, TRUE);
    return $this;
  }

  /**
   * removes an item form the index.
   *
   * @PARAM $item_id : an int that represents the id of the item in the $index
   * @PARAM $index : a SearchApiIndex object 
   */
  protected function deleteItem($item_id, $index) {
    $uri = $this->getURI($item_id, $index);
    entity_delete('resource_type', $uri);
    $num_deleted = db_delete('structsearchapi_uri')
      ->condition('item_id', $item_id)
      ->condition('index_id', $index->id)
      ->execute();
  
  }

  /**
   * inserts an item in the index
   *
   * @PARAM: an $entity object, both dataset and uri should be set
   */
  protected function insertItem($entity) {
    $result = entity_save('resource_type', $entity);
    return $result;
  }
  protected function getCreateFieldCB($type) {
    return isset($this->createFieldCBs[$type]) ?
           $this->createFieldCBs[$type] :
           'THEREISNOFUNCTIONOFTHISNAME';
  }

  /**
   * implements SearchApiQueryInterface::indexitems
   *
   * For each item we construct a Resource Entity and then use Entity Save
   * to add it to the ccr
   */
  public function indexItems(SearchApiIndex $index, array $items) {
    $results = array();
    foreach($items as $id => $item) {
      $entity = new stdClass();
      
      //go though the item and add the field that are struct field, and have
      //a type we know how to process
      foreach($item as $name => $field) {
        $cb = structsearchapi_get_create_field_cb($field['type']);
        if(function_exists($cb) && isset($field['struct_field'])) {
          $entity->{$field['struct_field']} = $cb($field);
        }
      }
      // add the dataset (from this service) to the entity this could be overridden by the item.
      if(!isset($entity->dataset) || !$entity->dataset) {
        $entity->dataset =$this->options['dataset'];
      }
     
      //only index if the uri match or we do not yet have a uri
      $uri = $this->getURI($id, $index) ? $this->getURI($id, $index) : $entity->uri;
      if($entity->uri && $entity->uri == $uri) {
        //if we do not have a uri lets save one.
        if(!$this->getURI($id, $index)) {
          $this->setURI($id, $uri, $index);
        }
        // use struct entity to save to ccr
        $action = $item['struct_action']['value'];
        switch($action) {
          case STRUCTSEARCHAPIINSERT:
            $this->debug($entity, "Insert $uri");
            $result = $this->insertItem($entity);
            if($result) {
              $results[] = $id;
            }
            else {
              watchdog("construct indexing", "could not save resource :uri", array(":uri" => $entity->uri));
            }
          break;
          case STRUCTSEARCHAPIREMOVE:
            $this->debug($entity, "Delete $uri");
            $this->deleteItem($id, $index);
            $results[] = $id;
          break;
        }
      }
    }

    return $results;
  }
  public function deleteItems($ids = 'all', SearchApiIndex $index = NULL) {
    if($index) {
      if($ids == 'all') {
        $ids = array_keys($this->cacheURIs($index));
      }
    }
    foreach($ids as $id) {
      $this->deleteItem($id, $index);
    }
  }  

  /** 
   * UNUSED 
   *
   *   We do not have to deal with add Index or removing Index,
   *   as we are not telling the index server what files should exist.
   *
   */
  public function search(SearchApiQueryInterface $query) { }
  public function addIndex(SearchApiIndex $index) { }
  public function fieldsUpdated(SearchApiIndex $index) { }  
  public function removeIndex($index) { }    

}

class SearchApiStructIndexProcessor extends SearchApiAbstractProcessor {

  protected $struct_properties = array("label" => "Label", 'uri' => "URI", 'dataset' => 'Dataset','network' => 'Network');
  /**
   * We need to find the dataset to use.
   *
   * We need to make the indexed field to resource fields.
   */
  public function configurationForm() {
    $mapping_options = array();

    //we get resource types as well as the ids of those that are not ignored.
    $resource_types = resource_types();
    $used_resource_types = structentities_get_exposed_resource_type_ids();
     
    //we want values to keep the same structure as the form
    $form['#tree'] = TRUE;

    $form['field_mapping'] = array(
      '#type' => 'fieldset',
      '#title' => 'Resource Field Mapping',
      '#description' => 'Enter the field name (not title) for each field you want mapped to a struct entity field. Note that in general you have to map something to URI and dataset'
    );

    //build a list of all fields and properties of resource, so that we can map to them.
    $instances = field_info_instances("resource");
    $field_options = array("" => "Not Mapped") + $this->struct_properties;
    $status_options = array("" => "Index all");
    $in_resource = array();
    foreach($used_resource_types as $name) {
      foreach($instances[$name] as $id => $instance) {
        $in_resource[$id][] = $name;
        $field_options[$id] = $instance['label'] . " (". implode(", ", $in_resource[$id]) . ")";
      }
    }

    //Provide a field for mapping a indexed field to a resource field.
    $fields = $this->index->getFields();
    foreach($fields as $name => $field) {
      if(in_array($field['type'], structsearchapi_get_create_field_cb_types())) {
        $form['field_mapping'][$name] = array(
          '#type'=> 'select',
          '#title' => $field['name'],
          '#options' => $field_options, 
          '#default_value' => isset($this->options['field_mapping'][$name]) ? $this->options['field_mapping'][$name]: "",
        );
      }
      if($field['type'] == 'boolean') {
        $status_options[$name] = $field['name'];
      }
      $mapping_options[$name] = $field['name'];
    }

    $form['type_mapping'] = array(
      '#type' => 'fieldset',
      '#title' => 'Resource Type Mapping',
      '#description' => 'Map a single field to the resource type. First select the field to use, and then for each resource type enter a regex to match against the selected field.'
    );

    //provide a selector for which field should map to resource type
    $form['type_mapping']['field'] = array(
      '#type' => 'select',
      '#title' => 'Field to use for type Mapping',
      '#options' =>  $mapping_options,
        '#default_value' => isset($this->options['type_mapping']['field']) ? $this->options['type_mapping']['field']: "",
    );

    //for each resource type provide a field with a regex on the field selected about.
    foreach($used_resource_types as $name) {
      $type = $resource_types[$name];
      $form['type_mapping']['map'][$name] = array(
        '#type'=> 'textfield',
        '#title' => $type->label,
        '#default_value' => isset($this->options['type_mapping']['map'][$name]) ? $this->options['type_mapping']['map'][$name]: "",
      );
    }

    $form['status_mapping'] = array(
      '#type' => 'fieldset',
      '#title' => 'Status Mapping',
      '#description' => 'Which field should be used to determine if the item should be place in the index, only boolean fields are presented'
    );
    //provide a selector for which field should map to resource type
    $form['status_mapping']['field'] = array(
      '#type' => 'select',
      '#title' => 'Field to use for type Mapping',
      '#options' =>  $status_options,
        '#default_value' => isset($this->options['status_mapping']['field']) ? $this->options['status_mapping']['field']: "",
    );
    
    return $form;
  }

  public function configurationFormValidate(array $form, array &$values, array &$form_state) {
  }

  public function process(&$value) {
  }
  public function preprocessIndexItems(array &$items) {
    foreach($items as &$item) {
      $options = $this->options['field_mapping'];
      foreach($item as $name => &$data) {
        if (isset($options[$name]) && $options[$name]) {
          $data["struct_field"] = $options[$name];
          if(isset($this->struct_properties[$options[$name]])) {
            $data["type"] = 'property';
          }
        }
      }
      $type_field = $item[$this->options['type_mapping']['field']]['value'];
      foreach($this->options['type_mapping']['map'] as $type => $regex) {
        if ($regex && preg_match($regex, $type_field)) {
          $item['stuct_type'] = array('value' => $type, 'type' => 'property', 'struct_field' => 'type');
        }
      }
      $item['struct_action']['type'] = 'none';
      if($status_field = $this->options['status_mapping']['field']) {
        $item['struct_action']['value'] = $item[$status_field]['value'] ?
          STRUCTSEARCHAPIINSERT :
          STRUCTSEARCHAPIREMOVE ;
      }
      else {
        $item['struct_action']['value'] = STRUCTSEARCHAPIINSERT;
      }

    }
  }
}


/**
 * Search API data alteration callback that adds the struct uri field
 */
class SearchApiStructAddURIForNode extends SearchApiAbstractAlterCallback {

  public function alterItems(array &$items) {
    foreach ($items as $id => &$item) {
      $lang = $item->language;
      $item->uri = $this->getNodeURI($item->nid);
      foreach($this->getEntityRefFields() as $name => $field) {
        $data = array($lang=>array());
        if(isset($item->$name[$lang])) {
          foreach($item->$name[$lang] as $row) {
            $data[] = array("value"=>$this->getNodeURI($row['target_id']));
          }
          $item->{"uri_$name"}=$data;
        }
      }
      foreach($this->getEntityRefFields('dataset') as $name => $field) {
        $lang = field_language("node", $item, $name);
        if(isset($item->$name[$lang])) {
          $item->{"dataset_for_$name"} = structsearchapi_dataset_get_dataset($item->{$name}[$lang][0]['target_id']);
          $item->{"network_for_$name"} =  structsearchapi_dataset_get_network($item->{$name}[$lang][0]['target_id']);
        }
      }
    }
  }
  
  /**
   * return a cononical uri for a node
   */
  protected function getNodeURI($nid) {
    return url("", array("absolute"=>TRUE)) . "node/". $nid;
  }

  /**
   * find all of the node reference fields
   */
  protected function getEntityRefFields($target_bundle = NULL) {
    $ret = array();
    foreach (field_info_fields() as $name => $field) {
      if ($field['type'] == 'entityreference' && 
          array_key_exists($this->index->item_type, $field['bundles']) &&
          ($field['settings']['target_type'] == 'node')) {
        if($target_bundle) {
          if(in_array($target_bundle, $field['settings']['handler_settings']['target_bundles'])) {
            $ret[$name] = $field;
          }
        }
        else {
          $ret[$name] = $field;
        }
      }
    }
    return $ret;
  }
 
  /**
   * implements propertyInfo()
   */
  public function propertyInfo() {
    $ret = array();
    if($this->index->item_type == "node") { 
      $ret['uri'] = array(
        'label' => t('Struct URI'),
        'description' => t('An URI where the item can be accessed.'),
        'type' => 'uri',
      );
    }
    $fields = self::getEntityRefFields();
    $dfields = self::getEntityRefFields('dataset');

    foreach ($fields as $name => $field) {
      $ret['uri_' . $name] = array(
        'label' => t("@field Struct URI", array("@field"=>$name)),
        'description' => t("An URI where the node reference can be accessed"),
        'type' => 'text',
      );
    }
    foreach ($dfields as $name => $field) {
      $ret['dataset_for_' . $name] = array(
        'label' => t("the dataset uri for @field ", array("@field"=>$name)),
        'description' => t("An URI of the refeanced dataset"),
        'type' => 'text',
      );
      $ret['network_for_' . $name] = array(
        'label' => t("the network uri for @field ", array("@field"=>$name)),
        'description' => t("An URI of the refeanced dataset's network"),
        'type' => 'text',
      );
    }
    return $ret;
  }

}

