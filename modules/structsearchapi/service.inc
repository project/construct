<?php

// Include struct code
use \StructuredDynamics\structwsf\php\api\ws\search\SearchQuery;
use \StructuredDynamics\structwsf\framework\Namespaces;
use \StructuredDynamics\structwsf\framework\Resultset;
use \StructuredDynamics\structwsf\framework\Subject;
use \StructuredDynamics\structwsf\php\api\ws\dataset\read\DatasetReadQuery;
use \StructuredDynamics\structwsf\php\api\ws\ontology\read\GetPropertyFunction;
use \StructuredDynamics\structwsf\php\api\ws\ontology\read\OntologyReadQuery;
use \StructuredDynamics\structwsf\php\api\ws\ontology\read\GetLoadedOntologiesFunction;
use \StructuredDynamics\structwsf\php\api\ws\crud\read\CrudReadQuery;

/**
 * Search service class that uses the structWSF service to retrieve data from.
 */
class SearchApiStructService extends SearchApiAbstractService {

  // Allow user to select which of the Networks to use for this search.
  public function configurationForm(array $form, array &$form_state) {

    // Get list of available networks from structnetworks.
    $networks = variable_get("WSF-Registry", array());

    // Put networks into options array ( key and value the same )
    $options = array();
    foreach ($networks as $network) {
      $options[$network] = $network;
    }

    // Network form select
    $form['network'] = array(
      '#type' => 'select',
      '#title' => t('Network'),
      '#description' => t('Choose a network to search against.'),
      '#options' => $options,
      '#default_value' => $this->options,
      '#required' => TRUE,
    );

    return $form;
  }

  // List of additional features that this Service supports.
  public function supportsFeature($feature) {
    $supported = drupal_map_assoc(array(
      'search_api_facets',
      'search_api_facets_operator_or',
    ));
    return isset($supported[$feature]);
  }

  // Method that does the actual search and controls pagination.
  public function search(SearchApiQueryInterface $query) {
    $time_method_called = microtime(TRUE);
    // Create the SearchQuery object
    $index = $query->getIndex();

    if (!empty($this->options['network'])) {
      $search = new SearchQuery($this->options['network']);
    }
    else {
      throw new SearchApiException(t('No network set when attempting query.'));
    }

    // Apply the search settings defined by the admin in the search settings UI
    _structsearchapi_apply_search_settings($search);    
    
    // Set some basic search params
    $search->mime("resultset");
    
    if(variable_get('structsearchapi_inference_enabled', 1))
    {
      $search->enableInference();
    }
    else
    {
      $search->disableInference();
    }

    $options = $query->getOptions();

    $namespaces = new Namespaces;
    
    // Set pagination vars
    $max = !empty($options['limit']) ? $options['limit'] : 20;
    $search->items($max);
    $page = !empty($options['offset']) ? $options['offset'] : 0;
    $search->page($page);

    // Ensure we have at least a dataset facet for getting the total count
    $key_hash = structsearchapi_facet_key_hash(Namespaces::$void . 'Dataset');
    if (empty($options['search_api_facets'][$key_hash])) {
      $options['search_api_facets'][$key_hash] = array(
        'field' => Namespaces::$void . 'Dataset',
        'limit' => '50',
        'operator' => 'and',
        'min_count' => 1,
        'missing' => 0,
      );
    }

    // Set the query parameter with the search keywords
    $query_keywords = &drupal_static('structsearchapi_keywords', _structsearchapi_sanitize_keywords($this->queryKeywords($query)));

    // If no valid keywords were submitted, return an empty result set
    if(empty($query_keywords)) {
      $results = '';
      return array(
        'results' => array(),
        'search_api_facets' => array(Namespaces::$void . 'Dataset' => array()),
        'result count' => 0,
        'performance' => array(),
      );
    }


    $search->query($query_keywords);
    $search->includeSpellcheck();

    // Set facets
    $filters = $query->getFilter()->getFilters();
    $datasets = array();
    $types = array();
    $properties = array();
    // Filter by facet
    if (!empty($filters)) {
      foreach ($filters as $filter) {
        if (empty($properties[$filter[0]])) {
          $properties[$filter[0]] = array();
        }

        $values = array();

        if(is_string($filter[1])) {
           $values = '"'. trim($filter[1]) .'"';
        }
        else {
          foreach ($filter[1] as $value) {
            $values[] = $value;
          }
        }

        $properties[$filter[0]][] = $values;
      }
    }

    $this->setFacets($this->options['network'], $filters, $datasets, $types, $properties);

    $datasets += _structsearchapi_get_faceted_datasets();

    if (!empty($datasets)) {
      $search->datasetsFilters($datasets);
    }
    else {
      // If no datasets filters are specified, we have to limit the Search query to the
      // datasets, of that network, that are linked/existing in this conStruct portal.
      // Otherwise we could have situations where the users has access to multiple
      // datasets on the network, but these datasets were not exposed on that
      // particular conStruct portal. We handle this usecase by making sure that only
      // the exposed datasets are taken into account for that search query.
      $linkedDatasets = variable_get("Linked-Dataset-Registry", array());

      if (empty($linkedDatasets)) {
        drupal_set_message(t('There are no linked datasets.'), 'error');
      }

      $results = db_query('SELECT nid, did FROM {construct_datasets} WHERE wsf = :wsf', array(':wsf' => $this->options['network']));

      $datasets_list = variable_get('construct_linked_datasets', array());
      foreach ($datasets_list as $datasets_item) {
        if ($datasets_item) {
          $datasets[] = $datasets_item;
        }
      }

      $search->datasetsFilters($datasets);
    }
    if (!empty($types)) {
      $search->typesFilters($types);
    }
    if (!empty($properties)) {
      $search->attributesValuesFilters($properties);
    }

    $time_processing_done = microtime(TRUE);

    // Adds extra information needed for Facets API
    $search->setAggregateAttributesObjectTypeToUriLiteral();

    if (module_exists('search_api_facetapi')) {
      // Add the aggregate attributes -- a URI for each facet that appears with this search.
      $searcher = 'search_api@struct_search_index';
      $searcher = 'search_api@' . $index->machine_name;
      $adapter = facetapi_adapter_load($searcher);
      foreach ($adapter->getEnabledFacets() as $key => $value) {
        $search->aggregateAttribute($value['field']);
      }
    }

    $search
      ->includeAggregates()
      ->numberOfAggregateAttributesObject(-1);

    // Make sure that we only retrieve the records with the types
    // that have been synchronized by structEntities
    $classesByBundles = variable_get('structentities_classes_by_bundles', array());

    $search->typesFilters(array_values($classesByBundles));

    if (!empty($options['search_profile'])) {
      $sprofile = $options['search_profile'];

      // Add the settings from the sprofile to the search query
      foreach ($sprofile->settings as $setting => $value){

        // If it's an empty array
        if (empty($value)) {
          $search->$setting;
        }
        elseif (is_array(reset($value))) {
          // Otherwise, if the first item in $value is an array, it's multiple methods
          foreach ($value as $value_item) {
            call_user_func_array(array($search, $setting), $value_item);
          }
        }
        else {
          // And otherwise, it's one method
          call_user_func_array(array($search, $setting), $value);
        }
      }
      $search->items(20);
    }

    // Send the search query to the endpoint
    $search->query($query_keywords);
    $search->send(new DrupalQuerierExtension());

    $time_query_done = microtime(TRUE);

    if (!$search->isSuccessful()) {
      // Set a generic message that there was something wrong
      drupal_set_message(
        t('There was a problem searching for \'%keywords\' because there was a problem with the system. Please try your search again later.', array('%keywords' => $query_keywords)),
        'error'
      );

      // If the user is an admin that we trust, display a more helpful message
      if (user_access('administer construct')) {
        drupal_set_message(
          t('Search API Struct query error [%status] %msg (%msg_desc).',
          array(
            '%status' => $search->getStatus(),
            '%msg' => $search->getStatusMessage(),
            '%msg_desc' => $search->getStatusMessageDescription()
          )),
          'error'
        );
      }

      // Regardless of the user, log the error message details.
      watchdog(
        'StructSearchAPI',
        'Search API Struct query error [%status] %msg (%msg_desc).',
        array(
          '%status' => $search->getStatus(),
          '%msg' => $search->getStatusMessage(),
          '%msg_desc' => $search->getStatusMessageDescription()
        ),
        WATCHDOG_WARNING
      );

      // return an empty array rather than a PHP exception so that the page will still return.
      return array();
    }

    // Get back the resultset returned by the endpoint and make it statically
    // accessible for other modules to work with this result set
    //$resultset = &drupal_static('structsearchapi_resultset', $resultset = $search->getResultset()->getResultset());
    $resultset_temp = $search->getResultset()->getResultset();
    $resultset = &drupal_static('structsearchapi_resultset', $resultset = $search->getResultset()->getResultset());
    $resultset = $resultset_temp;

    // Iterate over resultset.  The only thing that matters here is $uri ( I think ), the other items get filled in by the loadItems method in datasource.inc
    $results = array();
    $results['results'] = array();

    if (!empty($resultset)) {
      foreach ($resultset as $items) {
        foreach ($items as $uri => $item) {

          // Skip the Aggregate records that are part of the resultset.
          if(empty($item['type']) || $item['type'][0] == Namespaces::$aggr."Aggregate") {
            continue;
          } else if ($item['type'][0] == Namespaces::$wsf."SpellSuggestion") {
            if (empty($results['spell_correction']) && !empty($item[Namespaces::$wsf."suggestion"])) {
              $results['spell_correction'] = $item[Namespaces::$wsf."suggestion"][0]['value'];
            }
            continue;
          }

          $result = array(
            'id' => $uri,
            'type' => 'structwsf',
          );

          $results['results'][$uri] = $result;
        }
      }
    }

    // Facets
    $resultset['properties'] = $properties;
    $results['search_api_facets'] = $this->getFacets($resultset, $this->options['network'], $options['search_api_facets']);

    // Make sure we have a Dataset facet by default
    if (empty($results['search_api_facets'][Namespaces::$void . 'Dataset'])) {
      //$results['search_api_facets'][Namespaces::$void . 'Dataset'] =
    }

    // Calculate the number of results for that search query
    $total_items = 0;
    if (!empty($results['search_api_facets'][Namespaces::$void . 'Dataset'])) {
      foreach($results['search_api_facets'][Namespaces::$void . 'Dataset'] as $dataset) {
        $total_items += $dataset['count'];
      }
    }

    $results['result count'] = $total_items;

    // Compute performance
    $time_end = microtime(TRUE);
    $results['performance'] = array(
      'complete' => $time_end - $time_method_called,
      'preprocessing' => $time_processing_done - $time_method_called,
      'execution' => $time_query_done - $time_processing_done,
      'postprocessing' => $time_end - $time_query_done,
    );

    return $results;
  }

  /* Helper function to add a keyword list to the query */
  public function queryKeywords(SearchApiQueryInterface $query) {
    $q_string = '';
    $keys = $query->getKeys();
    foreach ($keys as &$key) {
      if (strpos($key, ' ')) {
        $key = '"' . $key . '"';
      }
    }

    if (is_array($keys)) {
      $conjunction = $keys['#conjunction'];
      unset($keys['#conjunction']);   // Remove key from array
      if (strtoupper($conjunction) == 'AND') {
        $q_string = implode(' ', $keys);
      }
      else {
        $q_string = implode(' OR ', $keys);
      }
    }
    return $q_string;
  }

  /* Helper function to retrieve the facets from a query and create the data for facet blocks */
  public function getFacets($results, $network, $requested_facets) {
    $facets = array();
    $datasets = array();
    $classes = array();
    // Retrieve facets from results. Type and Dataset are always available in these
    foreach ($results as $dataset => $aggregates) {
      foreach ($aggregates as $uri => $aggregate) {
        if (empty($aggregate['type']) || $aggregate['type'][0] != Namespaces::$aggr . 'Aggregate') {
          continue;
        }

        if ($aggregate[Namespaces::$aggr . 'property'][0]['uri'] == Namespaces::$void . 'Dataset') {
          array_push($datasets, array($aggregate[Namespaces::$aggr . 'object'][0]['uri'], $aggregate[Namespaces::$aggr . 'count'][0]['value']));
        }

        if ($aggregate[Namespaces::$aggr . 'property'][0]['uri'] == Namespaces::$rdf . 'type') {
          array_push($classes, array($aggregate[Namespaces::$aggr . 'object'][0]['uri'], $aggregate[Namespaces::$aggr . 'count'][0]['value']));
        }
      }
    }

    // Datasets
    $dataset_counts = array();
    foreach ($datasets as $dataset) {
      $dataset_counts[$dataset[0]] = array(
        'filter' => '"'.$this->getDatasetTitle($dataset[0]).'"',
        'count' => $dataset[1],
      );
    }

    // Classes ( Types )
    $class_counts = array();
    foreach ($classes as $class) {
      $classes = variable_get("structentities_classes", array());
      $label = "";

      if(isset($classes[$class[0]])) {
        $classSubject = unserialize($classes[$class[0]]);
        $label = $classSubject->getPrefLabel();

        if($label == "") {
          $label = structsearchapi_get_label_from_uri($class[0]);
        }
      }
      else {
        $label = structsearchapi_get_label_from_uri($class[0]);
      }

      $class_counts[$class[0]] = array(
        'filter' => '"'.$label.'"',
        'count' => $class[1]
      );
    }

    // Get any other facets which were requested but which aren't in aggregated data by default
    if (!empty($requested_facets)) {
      foreach ($requested_facets as $fid => $facet_info) {
        $key = $facet_info['field'];
        switch ($facet_info['field']) {
          case Namespaces::$void . 'Dataset':
            $facets[$key] = $dataset_counts;
            break;
          case Namespaces::$rdf . 'type':
            $facets[$key] = $class_counts;
            break;
          default:
            $counts = $this->getFacetCounts($facet_info['field'], $results['properties']);
            if (!empty($counts)) {
              if (!isset($facets[$key])) {
                $facets[$key] = array();
              }
              $facets[$key] += $counts;
            }
        }
      }
    }
    return $facets;
  }

  /* Apply the selected Facets to the query.  Unfortunately, the facetapi gives us the name ( prefLabel ) of the facet, and we need to pass the uri into the search object.
     Thus, the additional query */
  public function setFacets($network, $filters, &$datasets, &$types, &$properties) {
    if (!empty($filters)) {

      $facetsPropertiesUris = cache_get('strucsearchapi_facets_properties_uris_labels');
      $facetsPropertiesUris = $facetsPropertiesUris->data;
      $facetsClassesUris = cache_get('strucsearchapi_facets_classes_uris_labels');
      $facetsClassesUris = $facetsClassesUris->data;
      $facetsDatasetsUris = cache_get('strucsearchapi_facets_datasets_uris_labels');
      $facetsDatasetsUris = $facetsDatasetsUris->data;

      foreach ($filters as $filter) {

        $datasetUri = array_search($filter[1], $facetsDatasetsUris);
        $propertyUri = array_search($filter[1], $facetsPropertiesUris);
        $classUri = array_search($filter[1], $facetsClassesUris);

        if ($filter[0] == 'struct_dataset' && $datasetUri !== FALSE) {
          array_push($datasets, $datasetUri);
        }
        elseif ($filter[0] == 'struct_type' && $classUri !== FALSE) {
          array_push($types, $classUri);
        }
        elseif ($filter[0] == 'struct_property' && $propertyUri !== FALSE) {
          $properties[$propertyUri] = array();
        }
      }
    }
  }

  private function getDatasetTitle($datasetURI) {
    $nid = db_query('SELECT nid FROM {construct_datasets} where did = :did', array(':did' => $datasetURI))->fetchField();
    $title = db_query('SELECT title FROM {node} where nid = :nid', array(':nid' => $nid))->fetchField();

    if($title == "") {
      // Check if it is a linked dataset
      $linkedDatasets = variable_get("Linked-Dataset-Registry", array());

      foreach($linkedDatasets as $nid => $uri) {
        if($uri == $datasetURI) {
          $title = db_query('SELECT title FROM {node} where nid = :nid', array(':nid' => $nid))->fetchField();
        }
      }

      if($title == "") {
        $title = $datasetURI;
      }
    }

    return($title);
  }

  /*
   * For a given property ($facet_property_key), perform an aggregate request and
   * return values and counts keyed as facet_api expects
   */
  private function getFacetCounts($facet_property_key, $properties = array()) {
    $is_data_property = FALSE;
    $ob = construct_structproperty_info($facet_property_key);

    if ($ob) {
      $is_data_property = (array_search(Namespaces::$owl . 'ObjectProperty', $ob->getTypes()) === FALSE);
    }
    else {
      drupal_set_message(t('Unknown property type: @k, assuming object', array('@k' => $facet_property_key)), 'error');
      return array();
    }

    $query_keywords = &drupal_static('structsearchapi_keywords');
    $resultset = &drupal_static('structsearchapi_resultset');
    if (empty($query_keywords) || empty($resultset)) {
      return array();
    }

    $return = array();

    if (!empty($resultset['unspecified'])) {
      foreach ($resultset['unspecified'] as $property_key => $property_value) {
        if ($property_value['http://purl.org/ontology/aggregate#property'][0]['uri'] == $facet_property_key) {
          if ($is_data_property) {
            $key = $property_value['http://purl.org/ontology/aggregate#object'][0]['value'];
            $label = $key;
          }
          else {
            $key = $property_value['http://purl.org/ontology/aggregate#object'][0]['uri'];
            $label = $property_value['http://purl.org/ontology/aggregate#object'][1]['value'];
          }
          $count = $property_value['http://purl.org/ontology/aggregate#count'][0]['value'];

          $return[$key] = array(
            'filter' => '"'.$label.'"',
            'count' => $count,
          );
        }
      }
    }

    return $return;
  }

  /* UNUSED */
  /* Since we're not writing to the Network, these don't need to do anything. */
  public function addIndex(SearchApiIndex $index) { }
  public function fieldsUpdated(SearchApiIndex $index) { }
  public function removeIndex($index) { }
  public function indexItems(SearchApiIndex $index, array $items) { }
  public function deleteItems($ids = 'all', SearchApiIndex $index = NULL) { }
}


/**
 * Return an array of dataset URIs if the dataset facet is being used to limit
 * content to a specific dataset (or set of datasets).
 * @return array Array containing the faceted datasets to be used to limit by
 */
function _structsearchapi_get_faceted_datasets() {
  $datasets = array();
  $labels = array();

  // Filter by faceted datasets
  if (!empty($_GET['f'])) {
    foreach ($_GET['f'] as $f) {
      if (strpos($f, 'Dataset:') === 0) {
        if (empty($labels)) {
          $labels = cache_get('strucsearchapi_facets_datasets_uris_labels');
          $labels = array_flip($labels->data);
        }
        $label = str_replace('Dataset:', '', $f);
        if (strpos($label, 'http') === 0) {
          $datasets[] = $label;
        }
        elseif (!empty($labels[$label])) {
          $datasets[] = $labels[$label];
        }
      }
    }
  }

  return $datasets;
}

function _structsearchapi_sanitize_keywords($keywords) {
  $keywords = str_replace('?', '', $keywords);
  return $keywords;
}
