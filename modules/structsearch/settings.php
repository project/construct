<?php

/*! @defgroup StructSearchModule construct Search Drupal Module */
//@{
/*! @file structsearch/settings.php
     @brief Settings for the structbrowse module
     @author Frederick Giasson, Structured Dynamics LLC.
       \n\n\n
 */
// Ignore these "Kinds" when displaying the kinds to filter by
// @todo: creating a UI setting for this
$filterKindsIgnore = array();

// Ignore these "Attributes" when displaying the attributes to filter by
// @todo: creating a UI setting for this
$filterAttributesIgnore = array();

//@}
?>