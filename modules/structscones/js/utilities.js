jQuery(document).ready(function() {
  // Store our function as a property of Drupal.behaviors.
  Drupal.behaviors.structscones_utilities = {
    attach: function () {
  
      // Enable autocomplete for each input text
      jQuery('input')
        .filter(function() {
            if(this.id.match(/tag_literal__/))
            {
              var id = this.id.substring(13, this.id.length);
              
              initializeAutocomplete(id, jQuery);
            }
        });  
      
      
      if(window.reviewTag)
      {
        // Get the label of each tag from the controlled vocabulary
        var prefLabelAttributesString = "";
        
        for(var i = 0; i < window.prefLabelAttributes.length; i++)
        {
          prefLabelAttributesString += urlencode(window.prefLabelAttributes[i]) + ";";
        }
        
        for(var i = 0; i < 256; i++)
        {
          if (jQuery("#tag_uri__"+i).length > 0)
          {
            if(jQuery("#tag_uri__"+i).val() != "")
            {
              jQuery.ajax({                          
                type: "POST",
                url: structwsfNetworkUrl.replace(/\/+$/,"") + "/crud/read/",
                data: "uri="+jQuery("#tag_uri__"+i).val()+"&dataset="+datasetUri+"&include_attributes_list="+prefLabelAttributesString,
                dataType: "json",
                tagLiteralId: "#tag_literal__"+i,
                success: function(msg)
                {
                  // Process the resultset
                  resultset = new Resultset(msg);
                  
                  jQuery(this.tagLiteralId).text(resultset.subjects[0].getPrefLabel());
                  
                  
                  /*
                  for(var i = 0; i < resultset.subjects.length; i++)
                  {
                    loadedOntologiesNames[resultset.subjects[i].uri] = resultset.subjects[0].getPrefLabel();
                  }

                  // Change the name of the ontology in the breadcrumb
                  jQuery("#breadCrumbOntologyName").text(loadedOntologiesNames[ontology]); */
                },
                error: function(jqXHR, textStatus, error)
                {  
                }
              });      
            }
          }
        }
      }
    }
  };
}(jQuery));

function initializeAutocomplete(id)
{
  var inputAc = jQuery("#tag_literal__"+id).autocomplete({
      serviceUrl: structModulesBaseUrl.replace(/\/+$/,"") + "/proxy/",
      structNetworkUrl: structwsfNetworkUrl.replace(/\/+$/,"") + "/search/",
      minChars: 3,
      maxHeight: 400,
      width: 300,
      zIndex: 9999,
      inputId: id,
      iconID: id,
      deferRequestBy: 0, //miliseconds
      params: {                                                 
        datasets: datasetUri,
        attributes: "http://purl.org/ontology/iron#prefLabel",
        types: "http://www.w3.org/2002/07/owl#Class",
        page: 0,
        items: 20,
        include_aggregates: "false"//,
        //types: "http://www.w3.org/2002/07/owl#Class"
      },
      noCache: false, //default is false, set to true to disable caching
      onSelect: function(value, data)
      {
        
        jQuery("#td_tag_"+data.inputId).empty();
        jQuery("#td_tag_"+data.inputId).append('<img src="'+localImgPath+'bullet_blue.png" style="padding-right: 3px; position: relative; top: 3px;" /><span id="tag_literal__'+data.inputId+'" class="tagSpan">'+value+'</span>');
        jQuery("#td_tag_"+data.inputId).append('<input id="tag_uri__'+data.inputId+'" type="hidden" value="'+data.uri+'" name="tag_uri__'+data.inputId+'" style="width:100%">');
        jQuery("#td_tag_"+data.inputId).append('<img title="Remove tag"\
                                                src="'+localImgPath+'delete.png"\
                                                style="margin-left:5px; cursor: pointer; margin-top: 3px;"\
                                                onclick="removeTag('+data.inputId+');" />');

                                                 
        jQuery("#addedTagsTable").append('<tr>\
            <td align="left" valign="top" id="td_tag_'+(parseInt(data.inputId)+1)+'">\
              <br /><input type="text" value="" id="tag_literal__'+(parseInt(data.inputId)+1)+'" name="tag_literal__'+(parseInt(data.inputId)+1)+'" style="width:350px;">\
              <input type="hidden" value="" id="tag_uri__'+(parseInt(data.inputId)+1)+'" name="tag_uri__'+(parseInt(data.inputId)+1)+'" style="width:350px;">\
            </td>\
          </tr>'); 
          
          initializeAutocomplete((parseInt(data.inputId)+1));
                                                 
        /*switch(currentView)
        {
          case "classes":
            jQuery("#classesTree").jstree("search", value.substring(0, value.search("&nbsp;&nbsp;&nbsp;")));
            break;

          case "properties":
            jQuery("#propertiesTree").jstree("search", value.substring(0, value.search("&nbsp;&nbsp;&nbsp;")));
            break;
        }*/
      }
    });

    inputAc.enable();  
}

function removeTag(tagNum)
{
  jQuery('#td_tag_'+tagNum).fadeOut('slow', function() {
    jQuery('#td_tag_'+tagNum).empty();
  });
}

function urlencode(str)
{
// http://kevin.vanzonneveld.net
// +   original by: Philip Peterson
// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// +      input by: AJ
// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// +   improved by: Brett Zamir (http://brett-zamir.me)
// +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// +      input by: travc
// +      input by: Brett Zamir (http://brett-zamir.me)
// +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// +   improved by: Lars Fischer
// +      input by: Ratheous
// +      reimplemented by: Brett Zamir (http://brett-zamir.me)
// +   bugfixed by: Joris
// +      reimplemented by: Brett Zamir (http://brett-zamir.me)
// %          note 1: This reflects PHP 5.3/6.0+ behavior
// %        note 2: Please be aware that this function expects to encode into UTF-8 encoded strings, as found on
// %        note 2: pages served as UTF-8
// *     example 1: urlencode('Kevin van Zonneveld!');
// *     returns 1: 'Kevin+van+Zonneveld%21'
// *     example 2: urlencode('http://kevin.vanzonneveld.net/');
// *     returns 2: 'http%3A%2F%2Fkevin.vanzonneveld.net%2F'
// *     example 3: urlencode('http://www.google.nl/search?q=php.js&ie=utf-8&oe=utf-8&aq=t&rls=com.ubuntu:en-US:unofficial&client=firefox-a');
// *     returns 3: 'http%3A%2F%2Fwww.google.nl%2Fsearch%3Fq%3Dphp.js%26ie%3Dutf-8%26oe%3Dutf-8%26aq%3Dt%26rls%3Dcom.ubuntu%3Aen-US%3Aunofficial%26client%3Dfirefox-a'
  str = (str + '').toString();

// Tilde should be allowed unescaped in future versions of PHP (as reflected below), but if you want to reflect current
// PHP behavior, you would need to add ".replace(/~/g, '%7E');" to the following.
  return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g,
    '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}