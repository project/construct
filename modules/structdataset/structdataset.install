<?php

define('STRUCTDATASET_MINIMUM_PHP_MEMORY', '128M');

/**
 * Implements hook_requirements().
 *
 * Ensure minimum memory requirements are met.
 */
function structdataset_requirements($phase) {
  $requirements = array();
  $t = get_t();

  $memory_limit = ini_get('memory_limit');
  $requirements['structdataset_memory_limit'] = array(
    'title' => $t('StructDataset PHP memory limit'),
    'value' => $memory_limit == -1 ? t('-1 (Unlimited)') : $memory_limit,
  );
  if ($memory_limit && $memory_limit != -1 && parse_size($memory_limit) < parse_size(STRUCTDATASET_MINIMUM_PHP_MEMORY)) {
    $description = $t('Consider increasing your PHP memory limit to %minimum_php_memory to help prevent errors when running the grammar parser.', array('%minimum_php_memory' => STRUCTDATASET_MINIMUM_PHP_MEMORY));

    if ($php_ini_path = get_cfg_var('cfg_file_path')) {
      $description .= ' ' . $t('Increase the memory limit by editing the memory_limit parameter in the file %configuration-file and then restart your web server (or contact your system administrator or hosting provider for assistance).', array('%configuration-file' => $php_ini_path));
    }
    else {
      $description .= ' ' . $t('Contact your system administrator or hosting provider for assistance with increasing your PHP memory limit.');
    }

    $requirements['structdataset_memory_limit']['description'] = $description;
    $requirements['structdataset_memory_limit']['severity'] = REQUIREMENT_WARNING;
  }

  return $requirements;
}