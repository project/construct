<?php
/**
 * @file
 * Code for the Struct Suggest feature.
 */

include_once 'structsuggest.features.inc';

/**
 * Implements hook_menu()
 *
 * Add a suggestions review page.
 */
function structsuggest_menu() {
  $items['construct/suggest/submit'] = array(
    'page callback' => 'structsuggest_submit_callback',
    'title' => 'New Suggestion',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );
  $items['construct/suggest/review/%node'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('structsuggest_review_form'), 
    'title' => 'Suggestion Resolution',
    'type' => MENU_CALLBACK,
    'access arguments' => array('administer nodes'),
  );
  $items['construct/suggest/review'] = array(
    'page callback' => 'structsuggest_review_page',
    'title' => 'Review Suggestions',
    'type' => MENU_CALLBACK,
    'access arguments' => array('administer nodes'),
  );
  return $items;
}

/**
 * Page callback for the suggestions review page.
 */
function structsuggest_review_page() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'suggestion')
    ->propertyOrderBy('created', 'DESC')
    ->range(0, 100)
    ->addMetaData('account', user_load(1)); // Run the query as user 1.
  $result = $query->execute();

  if (isset($result['node'])) {
    $suggestion_nids = array_keys($result['node']);
    $suggestions = entity_load('node', $suggestion_nids);
  }

  if (empty($suggestions)) {
    return t('No suggestions found.');
  }

  $header = array('Date', 'Ontology', 'Type', 'Label', 'Description', 'Host', 'User', 'Status', 'Review');
  foreach ($suggestions as $suggestion) {
    $rows[] = array(
      date('M j Y', $suggestion->created),
      $suggestion->field_existing_dataset_uri[LANGUAGE_NONE][0]['value'],
      $suggestion->field_suggestion_type[LANGUAGE_NONE][0]['value'],
      $suggestion->title,
      $suggestion->field_suggestion_change[LANGUAGE_NONE][0]['value'],
      $suggestion->field_suggestion_host[LANGUAGE_NONE][0]['value'],
      l($suggestion->field_suggestion_username[LANGUAGE_NONE][0]['value'], 'mailto:' . $suggestion->field_suggestion_usermail[LANGUAGE_NONE][0]['value'], array('absolute' => TRUE)),
      $suggestion->field_suggestion_status[LANGUAGE_NONE][0]['value'],
      l('Review', 'construct/suggest/review/' . $suggestion->nid),
    );
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}

function structsuggest_review_form($form, &$form_state) {
  $nid = arg(3);
  $suggestion = node_load($nid);
  $title = $suggestion->title;
  $suggestion->title = '';
  drupal_set_title(t('Review Suggestion: ') . $title);
  $suggestion_output = drupal_render(node_view($suggestion));
  $suggestion->title = $title;
  $form['suggestion'] = array(
    '#markup' => $suggestion_output . '<hr />',
  );
  $form['status'] = array(
    '#type' => 'select',
    '#options' => array(
      'accepted' => t('Accepted'),
      'parked' => t('Parked'),
      'rejected' => t('Rejected'),
    ),
    '#title' => t('Resolution'),
    '#required' => TRUE,
  );
  $form['notes'] = array(
    '#type' => 'textarea',
    '#title' => t('Resolution Notes'),
    '#required' => TRUE,
  );
  $form['tags'] = array(
    '#type' => 'textfield', 
    '#title' => t('Tags'), 
    '#autocomplete_path' => 'taxonomy/autocomplete/field_suggestion_tags',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Resolve'),
  );
  return $form;
}

function structsuggest_review_form_submit(&$form, &$form_state) {
  $nid = arg(3);
  $node = node_load($nid);
  $node->field_suggestion_notes[LANGUAGE_NONE][0]['value'] = $form_state['values']['notes'];
  $node->field_suggestion_status[LANGUAGE_NONE][0]['value'] = $form_state['values']['status'];
  if (!empty($form_state['values']['tags'])) {
    $tags = explode(',', $form_state['values']['tags']);
    foreach ($tags as $tag) {
      $term = taxonomy_get_term_by_name(trim($tag));
      if (!empty($term)) {
        $tid = $term[1]->tid;
      } else {
        $term = new stdClass();
        $vocabs = taxonomy_vocabulary_get_names();
        $vid = $vocabs['suggestion_tags']->vid;
        $term->name = trim($tag);
        $term->vid = $vid;
        taxonomy_term_save($term);
        $tid = $term->tid;
      }
      $node->field_suggestion_tags[LANGUAGE_NONE][] = array(
        'tid' => $tid,
      );
    }
  }

  node_save($node);
  drupal_set_message(t('This suggestion has successfully been resolved. ') . l('Go back...', 'construct/suggest/review'));
}

function structsuggest_submit_callback() {
  global $user;
  $user = user_load(1);
  $data = $_POST['suggestion'];
  $suggestion = new stdClass();
  $suggestion->type = 'suggestion';
  $suggestion->status = 1;
  $suggestion->title = $data['label'];
  $suggestion->field_existing_dataset_uri[LANGUAGE_NONE][0]['value'] = $data['ontology'];
  $suggestion->field_suggestion_type[LANGUAGE_NONE][0]['value'] = $data['type'];
  $suggestion->field_suggestion_change[LANGUAGE_NONE][0]['value'] = $data['change'];
  $suggestion->field_suggestion_host[LANGUAGE_NONE][0]['value'] = $data['hostsite'];
  $suggestion->field_suggestion_node[LANGUAGE_NONE][0]['value'] = $data['node'];
  $suggestion->field_suggestion_username[LANGUAGE_NONE][0]['value'] = $data['username'];
  $suggestion->field_suggestion_usermail[LANGUAGE_NONE][0]['value'] = $data['usermail'];
  $suggestion->field_suggestion_status[LANGUAGE_NONE][0]['value'] = 'unreviewed';
  node_save($suggestion);
  $user = user_load(0);
}
