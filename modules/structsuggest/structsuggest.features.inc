<?php
/**
 * @file
 * structsuggest.features.inc
 */

/**
 * Implements hook_node_info().
 */
function structsuggest_node_info() {
  $items = array(
    'suggestion' => array(
      'name' => t('Suggestion'),
      'base' => 'node_content',
      'description' => t('A user suggestion for adding or altering a term.'),
      'has_title' => '1',
      'title_label' => t('Term name'),
      'help' => '',
    ),
  );
  return $items;
}

