(function ($) {

  Drupal.behaviors.structSearchSuggest = {
    attach: function (context, settings) {
      var showPopup = function() {
        $('.suggestions-popup').removeClass('suggestions-hidden');
        $('.suggestions-popup').addClass('suggestions-loading');
      }
      var hidePopup = function() {
        $('.suggestions-popup').addClass('suggestions-hidden');
        $('.suggestions-popup').removeClass('suggestions-loading');
      }
      var showPopupContent = function(content) {
        $('.suggestions-popup').removeClass('suggestions-hidden');
        $('.suggestions-popup').removeClass('suggestions-loading');
        $('.suggestions-content').html(content);
      }

      var request;

      $('#search-api-page-search-form-structwsf-search #edit-keys-1', context).keyup(function () {
        var query = $('#search-api-page-search-form-structwsf-search #edit-keys-1').val();
        if (query.length > 2) {
          if (request) {
            request.abort();
          }
          showPopup();

          request = $.get('ajax/structsearchsuggest/' + encodeURIComponent(query), function(data) {
            showPopupContent(data);
          });
          
        } else {
          hidePopup();
        }
      });

      $('body').click(hidePopup);
    }
  };

})(jQuery);
