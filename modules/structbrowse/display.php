<?php



/*! @defgroup StructBrowseModule construct Browse Drupal Module */
//@{

/*! @file structbrowse/display.php
     @brief Templated result in browser resultsets
     @details 	It is the place where results in browsing resultsets can be templated.
               The templating is performed given two different selection criterias: the
               type of the record being processed and the dataset where it comes from.

     @author Frederick Giasson, Structured Dynamics LLC.

       \n\n\n
 */

$nbPreviewItems = 0;
$addFromSkin = 0;

// Aggregating data used to display templated information about the result.

switch ($result["dataset"])
{
  /*
   * Create templates, for results of the resultset, per dataset here
  case "":

  break;

  */

  default:
    $preview = "";
    foreach ($result as $property => $value)
    {
      if ($property == "type" && isset($classHierarchy->classes[$value[0]])
        && $classHierarchy->classes[$value[0]]->label != "")
      {
        $preview .= "<img src=\"" . base_path() . drupal_get_path("module", "structbrowse")
          . "/imgs/bullet_blue.png\" border=\"0\" " .
          "style=\"padding-right:5px; position:relative; top:3px\" />" .
          "<em>is a</em> " . $classHierarchy->classes[$value[0]]->label . "<br />";

        continue;
      }

      if ($property == "dataset" || $property == "type")
      {
        continue;
      }

      foreach ($value as $text)
      {
        if ($text["text"] != "-" && $text["text"] != $resourceLabel && $text["text"] != "")
        {
          if ($nbPreviewItems < 10)
          {
            $propertyLabel = "";

            if (!isset($propertyHierarchy->properties[$property])
              || strlen($propertyLabel = $propertyHierarchy->properties[$property]->label) < 1)
            {
              $pos = strripos($property, "#");

              if ($pos === FALSE)
              {
                $pos = strripos($property, "/");
              }

              if ($pos !== FALSE)
              {
                $pos++;
              }

              $propertyLabel = substr($property, $pos, strlen($property) - $pos);
            }
            else
            {
              $propertyLabel = $propertyHierarchy->properties[$property]->label;
            }

            // remove HTML markup
            $text["text"] = strip_tags($text["text"]);

            $preview .= "<img src=\"" . base_path() . drupal_get_path("module", "structbrowse")
              . "/imgs/bullet_blue.png\" border=\"0\" " .
              "style=\"padding-right:5px; position:relative; top:3px\" />" .
              "<em>" . $propertyLabel . ":</em> "
              . (strlen($text["text"]) > 256 ? substr($text["text"], 0, 256) . "..." : $text["text"]) . " ";

            if ($text["uri"] != "" && strpos($text["uri"], "/irs/") === FALSE)
            {
              $preview .= "<a title=\"Page\" href=\"" . $text["uri"] . "\"><img title=\"Page\" border=\"0\" src=\""
                . base_path() . drupal_get_path("module", "structbrowse") . "/imgs/application_link.png\" /></a>";
            }

            $preview .= "<br />";

            $nbPreviewItems++;
          }
          else
          {
            $preview .= "<img src=\"" . base_path() . drupal_get_path("module", "structbrowse")
              . "/imgs/bullet_blue.png\" border=\"0\" " .
              "style=\"padding-right:5px; position:relative; top:3px\" />" .
              "And more ...<br />";
            break;
          }
        }
      }

      if ($nbPreviewItems >= 10)
      {
        break;
      }
    }
    break;
}


//@}

?>