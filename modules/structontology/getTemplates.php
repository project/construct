<?php

  /*! @defgroup StructOntologyModule construct Ontology Drupal Module */
  //@{ 

  /*! @file getTemplates.php
      @brief Get templates applicable to a target class
      @details This file returns a list of templates that can be used to display information about instance
               of that class. This is returned to the JavaScript application to display to the user in the 
               advance pannel.
   
      @author Frederick Giasson, Structured Dynamics LLC.

      \n\n\n
   */

  $currentDir = getcwd();

  // Change the directory of the executed script to enable Drupal to bootstrap
  chdir($_SERVER['DOCUMENT_ROOT']);

  include_once('./includes/bootstrap.inc');
  drupal_bootstrap(DRUPAL_BOOTSTRAP_SESSION);

  if(user_access('admin construct ontology'))
  {
    $templates = getTemplates();

    header("HTTP/1.1 200 OK");
    header("Content-Type: application/json");

    echo "{\n";

    echo "  \"templates\": [\n";

    for($i = 0; $i < count($templates); $i++)
    {
      if(($i + 1) == count($templates))
      {
        echo "\"" . $templates[$i] . "\"\n";
      }
      else
      {
        echo "\"" . $templates[$i] . "\",\n";
      }
    }

    echo "]}";
  }
  else
  {
    echo "{\n";

    echo "  \"templates\": [\n";

    echo "]}";    
  }


  /*! @brief Get all available templates defined on the node.

      \n

      @return returns an array of templates files defined on the node.

      @author Frederick Giasson, Structured Dynamics LLC.

      \n\n\n
  */
  function getTemplates()
  {
    $templates = array();

    if(is_dir(realpath(".") . "/" . drupal_get_path("module", "construct") . "/templates/"))
    {
      if($handle = opendir(realpath(".") . "/" . drupal_get_path("module", "construct") . "/templates/"))
      {
        while(($file = readdir($handle)) !== FALSE)
        {
          if($file != "." && $file != ".." && (stripos($file, ".html") !== FALSE || stripos($file, ".htm") !== FALSE
            || stripos($file, ".xhtml") !== FALSE))
          {
            array_push($templates, $file);
          }
        }
      }
    }

    return ($templates);
  }

  /*! @brief Get the Drupal user access for the requester.

      \n
      
      @author Frederick Giasson, Structured Dynamics LLC.

      \n\n\n
  */  
  function user_access($string, $account = NULL, $reset = FALSE)
  {
    global $user;
    static $perm = array();

    if($reset)
    {
      $perm = array();
    }

    if(!isset($account))
    {
      $account = $user;
    }

    // User #1 has all privileges:
    if($account->uid == 1)
    {
      return TRUE;
    }

    // To reduce the number of SQL queries, we cache the user's permissions
    // in a static variable.
    if(!isset($perm[$account->uid]))
    {
      $results = db_query("SELECT p.perm FROM {role} r INNER JOIN {permission} p ON p.rid = r.rid WHERE r.rid IN ("
        . db_placeholders($account->roles) . ")", array_keys($account->roles));

      $perms = array();

      foreach($results as $result)
      {
        $perms += array_flip(explode(', ', $result->perm));
      }
      
      $perm[$account->uid] = $perms;
    }

    return isset($perm[$account->uid][$string]);
  }

  function drupal_get_path($type, $name) { return dirname(drupal_get_filename($type, $name)); }
  
  
  //@}   
?>