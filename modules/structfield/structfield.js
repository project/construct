(function ($) {
  Drupal.behaviors.structfield = {
    attach: function (context, settings) {
      $('.field-widget-struct-lookup-field input', context).bind("refreshCheck", function() {
        
        if($(this).val().match(/^http/)) {
          $(this).trigger('refresh');
        }
      }).keydown(function () { //after something is press lets see if we need to refresh
        $(this).trigger("refreshCheck");
      }).blur(function () { // when we loose focus lets see if we should refresh
        $(this).trigger("refreshCheck");
      });
      //Add controls for selecting and clearing all checkboxes
      $(".structfield-select-all").click(function (e) {
        e.preventDefault();
        $(this).siblings(".form-type-checkboxes").find("input").attr("checked", "checked");
        $('.field-widget-struct-lookup-field .form-type-checkbox').addClass('structfield-selected');
      });
      $(".structfield-clear-all").click(function (e) {
        e.preventDefault();
        $(this).siblings(".form-type-checkboxes").find("input").removeAttr("checked");
        $('.field-widget-struct-lookup-field .form-type-checkbox').removeClass('structfield-selected');
      });
      $('.field-widget-struct-lookup-field .form-type-checkbox input', context).click(function () {
        /*  TODO make this only be on cardnality of one
        if($(this).attr("checked")) {
          $(this).parents(".field-widget-struct-lookup-field").find("input").not(this).removeAttr("checked").removeClass('structfield-selected');
        }
        */
      });
      $('.field-widget-struct-lookup-field .form-type-checkbox', context).each(function () {
        uri = $(this).find('input').val();
        rate = 1*Drupal.settings.structfield['field-name-field-test'][uri];
        size = 1 + rate;
        if(rate > 0) {
          $(this).find('label').css("font-size", size + 'em');
          $(this).css("line-height", size + 'em');
          $(this).find('input').css("top", -rate/4 + 'em');
          $(this).find('input').css("position", "relative");
        }
        else {
          $(this).addClass("no-scones");
        }
      }).click(function (e) {
        if($(this).find('input').attr('checked')) {
          $(this).addClass('structfield-selected');
        }
        else {
          $(this).removeClass('structfield-selected');
        }

      }).click();
      $('.ctools-use-modal').click(function() {
        $('.open-popup').removeClass('open-popup');
        $(this).addClass('open-popup');
      });

    }
  };


})(jQuery); 
