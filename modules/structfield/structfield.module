<?php
use \StructuredDynamics\structwsf\php\api\ws\scones\SconesQuery;
use \StructuredDynamics\structwsf\php\api\ws\crud\read\CrudReadQuery;
use \StructuredDynamics\structwsf\framework\Namespaces;
use \StructuredDynamics\structwsf\php\api\ws\search\SearchQuery;


/**
 * implements hook_menu()
 *
 * add a autocomplete callback url
 */
function structfield_menu() {
  $items['construct/field/get/%'] = array(
    'page callback' => 'structfield_text_autocomplete',
    'page arguments' => array(3,4),
    'access arguments' => array('view resource entities'),
    'type' => MENU_CALLBACK
  );
  $items['construct/suggest'] = array(
    'page callback' => 'structfield_suggest_form_page',
    'title' => 'Suggestion Form',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );
  return $items;
}

/**
 * Implements hook_field_widget_info().
 */
function structfield_field_widget_info() {
  return array(
    'struct_lookup_field' => array(
      'label' => t('Struct Lookup'),
      'settings' => array(
        'dataset'=>'file://localhost/data/ontologies/files/doha.owl', 
        'filter' => '',
        'autocomplete_count' => 20,
      ),
      'field types' => array('text'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      ),
    ),
    'struct_lookup_field_with_scones_suggest' => array(
      'label' => t('Struct Lookup With Suggest'),
      'settings' => array(
        'scones_fields'=>array(), 
        'dataset'=>'file://localhost/data/ontologies/files/doha.owl', 
        'filter' => '',
        'max_count' => 20,
        'autocomplete_count' => 20,
      ),
      'field types' => array('text'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      ),

    ),
    'struct_lookup_field_with_scones_tagging' => array(
      'label' => t('Struct Lookup With Tagging'),
      'settings' => array(
        'scones_fields'=>array(), 
        'dataset'=>'file://localhost/data/ontologies/files/doha.owl', 
        'filter' => '',
        'max_count' => 20,
        'autocomplete_count' => 20,
      ),
      'field types' => array('text'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      ),
    ),
  );
}

/**
 * implements hook_field_widget_settings_form()
 */
function structfield_field_widget_settings_form($field, $instance) {
  $wtype = $instance['widget']['type'];
  $fields = field_info_instances($instance['entity_type'], $instance['bundle']);
  //TODO this could be made more generic to pull properties from an entity
  //currently just looking for title
  $options = array('title::node_title' => t('title'));
  foreach($fields as $key => $field) {
    if(in_array($field['widget']['type'], structfield_acceptable_widgets())) {
      $options["$key::{$field['widget']['type']}"] = $field['label'];
    }
  }
  if($wtype == 'struct_lookup_field_with_scones_suggest' || $wtype == 'struct_lookup_field_with_scones_tagging') {
    $our_form['scones_fields'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $instance['widget']['settings']['scones_fields'],
      '#title' => t('Fields to include in SCONES lookup'),
    );
    $our_form['max_count'] = array(
      '#type' => 'select',
      '#default_value' => $instance['widget']['settings']['max_count'],
      //'#options' => array_slice(range(0, 64),1,64),
      '#options' => array(1,2,3,5,8,13,20,30,50),
      '#title' => t('Max number of suggestions'),
    );
  }
  $our_form['dataset'] = array(
    '#type' => 'textfield',
    '#default_value' => $instance['widget']['settings']['dataset'],
    '#description' => t('The dataset to which all of the tags belong'),
    '#title' => t('Dataset'),
  );
  $our_form['filter'] = array(
    '#type' => 'textfield',
    '#default_value' => $instance['widget']['settings']['filter'],
    '#description' => t('The type attruible to use while filtering the auto-complete options'),
    '#title' => t('Filter'),
  );
    $our_form['autocomplete_count'] = array(
      '#type' => 'select',
      '#default_value' => $instance['widget']['settings']['max_count'],
      '#options' => array(1,2,3,5,8,13,20,30,50),
      '#title' => t('Max number of options in autocomplete'),
    );
  return $our_form;
 
}

/**
 * implements hook_structfield_extract
 *
 * @RETURN: an array which list 2 callback to extract value, one from
 * a form state imput and from from a form as well  as the widget on which 
 * it works
 *
 * This are used to pull content out of fields on a form, so that it can be sent
 * to SCONES to get suggested tags
 * 
 */
function structfield_structfield_extract() {
  return array(
    array(
      'input_callback' => 'structfield_rec_extract_input',
      'form_callback' => 'structfield_rec_extract_form',
      'widgets' => array( 'text_textarea_with_summary'),
    ),
    array(
      'input_callback' => 'structfield_property_extract_input',
      'form_callback' => 'structfield_property_extract_form',
      'widgets' => array( 'node_title'),
    )
  );
}

/**
 * @RETURN: a list of all widget type that can be used in the scones lookup
 */
function structfield_acceptable_widgets() {
  $widgets = array();
  foreach(module_invoke_all("structfield_extract") as $item) {
    foreach ($item['widgets'] as $w) {
      $widgets[$w] = $w;
    } 
  }
  return $widgets;
}

/**
 * structfield_extract_values use the hook_structfield_extract system
 * to extract content from the fields on a drupal form.
 *
 * @PARAM $form: a drupal form
 * @PARAM $input: the input array from a drupal form_state
 * @PARAM $extract_identifiers: an array of strings of the from FIELDNAME::WIDGETTYPE
 * @RETURN a string containing all of the content for the FIELDNAME specified
 */
function structfield_extract_values($form, $input, $extract_identifiers) {
  $values = array();
  foreach($extract_identifiers as $extract_identifier) {
    $values = array_merge($values, _structfield_extract_value($form, $input, $extract_identifier));
  }
  return implode(" ", $values);
}

/**
 * a helper function for structfield_extract_values()
 */
function _structfield_extract_value($form, $input, $extract_identifier) {
  list($name, $widget) = explode("::", $extract_identifier);
  $values = array();
  if($extractors = structfield_get_extractors($widget)) {
    if($input) {
      $values = $extractors['input_callback']($input[$name]);
    }
    else {
      $values = $extractors['form_callback']($form[$name]);
    }
  }
  return $values;
}

/**
 * structfield_get_extractors find which extractor functions should be use for 
 * a particlar widget
 *
 * @PARAM $widget: the widget type
 * @RETURN : an array from hook_structfield_extract
 */
function structfield_get_extractors($widget) {
  $widgets = array();
  foreach(module_invoke_all("structfield_extract") as $item) {
    foreach ($item['widgets'] as $w) {
      $widgets[$w] = $item;
    }
  }

  return $widgets[$widget];
}

/**
 * a  hook_structfield_extract callback
 */
function structfield_rec_extract_input($tree) {
  return _structfield_rec_extract($tree, 'value');
}

/**
 * a  hook_structfield_extract callback
 */
function structfield_rec_extract_form($tree) {
  return _structfield_rec_extract($tree, '#default_value');
}

/**
 * a  hook_structfield_extract callback
 */
function structfield_property_extract_input($tree) {
  return array($tree);
}

/**
 * a  hook_structfield_extract callback
 */
function structfield_property_extract_form($tree) {
  return array($tree['#default_value']);
}

/**
 * search a element tree to find items of a paticular key
 *
 * @PARAM root_element : the element to transverse
 * @PARAM match_key : the key for which we are looking
 * @PARAM $ret : an array used for recurssion
 *
 * @return the values of all of the matched items
 */
function _structfield_rec_extract($root_element, $match_key = '#default_value', $ret = array()) {
  foreach($root_element as $key => $element) {
    if(!is_numeric($key) && ($key == $match_key)) {
      $ret[] = $element;
    }
    elseif(is_array($element)) {
      $ret = _structfield_rec_extract($element, $match_key, $ret);
    }
  }
  return $ret;
}

/**
 * Implements hook_field_widget_form().
 */
function structfield_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $form_id = $form_state['build_info']['form_id'];


  //register fields that effect suggestions
  $field_name = $instance['field_name'];
  $struct_config = array(
    'dataset' => $instance['widget']['settings']['dataset'],
    'filter' => $instance['widget']['settings']['filter'],
    'autocomplete_count' => $instance['widget']['settings']['autocomplete_count'],
    'label' => $instance['label'],
    'wtype' => $instance['widget']['type'],
    'cardinality' => $field['cardinality'],
    'lang' => $langcode
  );
 
  if(isset($instance['widget']['settings']['max_count'])) {
    $struct_config['max_count'] = $instance['widget']['settings']['max_count'];
  }
  if(isset($instance['widget']['settings']['scones_fields'])) {
    $struct_config['fields'] = $instance['widget']['settings']['scones_fields'];
  }
  _structfield_scones_fields($form_state, $field_name, $struct_config);

  $values = array();
  foreach($items as $item) {
    $values[] = $item['value'];
  }
  $element = array(
    '#title' => $instance['label'],
    '#type' => 'checkboxes',
    '#options' => array(),
    '#default_value' => $values,
    '#element_validate' => array('structfield_validate'),
    '#description' => $instance['description'],
    '#suffix' => "<a class= 'structfield-select-all' href='#'>select all</a> <a class = 'structfield-clear-all' href='#'>clear all</a>",
    '#attached' => array(
      'css' => array(drupal_get_path("module", 'structfield'). "/structfield.css"),
      'js' => array(drupal_get_path("module", 'structfield'). "/structfield.js"),
    ),
  );

  

  return $element;
}

/**
 * set the form_state to transfer info from widget create to the form alter
 */
function _structfield_scones_fields(&$form_state, $field = NULL, $config = NULL) {
  
  $form_id = $form_state['build_info']['form_id'];
  if ($form_id != 'field_ui_field_edit_form' && $field) {
    $form_state['structfield'][$field]  = $config;
  }
  if(isset($form_state['structfield'])) {
    return $form_state['structfield'];
  }
}

/**
 * implements hook_form_alter()
 *
 * We need to alter the form so that we can collect the field data
 * and pass it to scones
 */
function structfield_form_alter(&$form, &$form_state, $form_id) {
  if($struct_fields = _structfield_scones_fields($form_state)) {
    foreach($struct_fields as $struct_field => $config) {
      $lang = $config['lang'];
      $do_process = TRUE;
      //if this is one of our triggering events and it is not for this field lets stop processing
      //to save time
      if(isset($form_state['triggering_element']['#field']) && $form_state['triggering_element']['#field'] != $struct_field) {
        //TODO: This is causing issue with mulitple fields
        //$do_process = FALSE;
      }
      if($do_process) {
        //if we have a value in the selector that is a uri then lets add it as a checked
        //uri and clear the selector field
        if(isset($form_state['values'][$struct_field]['selector']) && 
           preg_match("/^http/", $form_state['values'][$struct_field]['selector'])) {
          $new  = $form_state['values'][$struct_field]['selector'];
          if($config['cardinality'] == 1) {
            foreach($form_state['input'][$struct_field][$lang] as $k => $v) {
              $form_state['input'][$struct_field][$lang][$k] = FALSE;
              
            }
          
          }
          $form_state['input'][$struct_field][$lang][$new] = $new;
          unset($form_state['input'][$struct_field]['selector']);
        }
       
        
        $uris = array();
        //lets grab the last set of urls we had.
        if(isset($form_state['struct_field_uris'][$struct_field])) {
          $uris = $form_state['struct_field_uris'][$struct_field];
        }
        $wtype = $config['wtype'];
        if($wtype == 'struct_lookup_field_with_scones_suggest' || $wtype == 'struct_lookup_field_with_scones_tagging') {
          // retrive content that should be sent to scones
          $doc = structfield_extract_values($form, $form_state['input'], $config['fields']);


          // lets not hit scones if the querying element does not want it.
          if(!isset($form_state['triggering_element']['#struct_field_no_suggestion'])) {
            //put the uris from cones if there is error tell the use and continue with no uri
            //suggestions
            try {
              $uris = _structfield_scones_request($doc, $config['max_count']);
            }
            catch(Exception $e) {
              drupal_set_message($e->getMessage(), 'error');
            }
          }
        }
        if (!empty($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][1] == 'refresh_related') {
          $networks = variable_get("WSF-Registry", array("http://localhost/ws/"));
          foreach ($networks as $network) {
            $search = new SearchQuery($network);
            $search->includeAggregates()
              ->aggregateAttribute(Namespaces::$dcterms.'subject')
              ->items(0)
              ->page(0)
              ->typeFilter(Namespaces::$bibo.'Document')
              ->enableInference()
              ->numberOfAggregateAttributesObject(20)
              ->setAttributesBooleanOperatorToOr()
              ->setAggregateAttributesObjectTypeToUriLiteral()
              ->attributeValuesFilters(Namespaces::$dcterms.'subject', array_keys($uris));
            $search->send(new DrupalQuerierExtension());
            $resultset = $search->getResultset();
            if (!empty($resultset)) {
              foreach($resultset->getResultset() as $dataset => $results) { 
                foreach($results as $uri => $result) {
                  if ($result['http://purl.org/ontology/aggregate#property'][0]['uri'] == 'http://purl.org/dc/terms/subject') {
                    $uris[$result['http://purl.org/ontology/aggregate#object'][0]['uri']] = $result['http://purl.org/ontology/aggregate#count'][0]['value'];
                  }
                }
              }
            }
          }
        }
        //get the current selected uri and insure they are part of the uri list
        $current_uris = isset($form_state['input'][$struct_field][$lang]) ?
          $form_state['input'][$struct_field][$lang] :
          $form[$struct_field][$lang]['#default_value'];
        foreach($current_uris as $c_uri) {
          if ($c_uri && !isset($uris[$c_uri])) {
            $uris[$c_uri] = t("No SCONES Match");
          }
        }
        $form_state['struct_field_uris'][$struct_field] = $uris;

        // get a map of the uri and there labels so we can display them
        // if there is an error let the user know and continue with the ones
        // we had cached and just uri for the rest
        if($wtype == 'struct_lookup_field_with_scones_suggest' || $wtype == 'struct_lookup_field_with_scones_tagging') {
          try {
            $map = _structfield_uri_label_map($uris, $config['dataset']);
          }
          catch(Exception $e) {
            drupal_set_message(t("not all labels returned because %error",array('%error' => $e->getMessage())), 'error');
            $map = _structfield_uri_label_map($uris, $config['dataset'], TRUE);
            foreach($uris as $uri => $label) {
              if (!isset($map[$uri])) {
                $map[$uri] = $uri;
              }
            }
          }

          //sort list alpha, and add count from scones before adding to field
          asort($map);
          $options = array();
          $scale = array();
          foreach($map as $uri => $label) {
            $options[$uri] = ($wtype == 'struct_lookup_field_with_scones_suggest' || $wtype == 'struct_lookup_field_with_scones_tagging') ?
              "$label ({$uris[$uri]})":
              $label;
            $max = max($uris);
            $scale[$uri] = is_numeric($uris[$uri]) ? $uris[$uri]/$max : 0;
            ctools_include('ajax');
            ctools_include('modal');
            ctools_modal_add_js();
            $query = array(
              'network' => variable_get('construct_OntologySettings_network', 'http://localhost/structwsf/ws'),
              'dataset' => $config['dataset'],
              'classUri' => $uri,
              'modal' => TRUE,
              'readonly' => TRUE,
            );
            $options[$uri] .= ' ' . l('(?)', 'construct/ontology/view', array('query' => $query, 'attributes' => array('class' => 'ctools-use-modal')));
          }
          $form[$struct_field][$lang]['#options'] = $options;
        }

        //move title to the container and make the container a fieldset
        $form[$struct_field]['#title'] = $form[$struct_field][$lang]['#title'];
        unset($form[$struct_field][$lang]['#title']);
        $form[$struct_field]['#type'] = 'fieldset';
        $form[$struct_field]['#attributes']['class'][] = 'field-widget-struct-lookup-field';

        $form[$struct_field]['#attached']['js'][] = array(
        'data' => array("structfield" => array('field-name-field-test' => $scale)),
        'type' => 'setting',
        );

        //wrapp field set for ajax calls
        $form[$struct_field]['#prefix']= "<div id= 'struct_field-term-holder-$struct_field'>";
        $form[$struct_field]['#suffix'] = "</div>";

        //add autocompete selector field
        $session_key = "for-{$form['#form_id']}-$struct_field";
        $_SESSION[$session_key] = $config;
        $form[$struct_field]['selector'] = array(
          '#type' => 'textfield',
          '#default_value' => '',
          '#autocomplete_path' => "construct/field/get/$session_key",
          '#weight' => -10,
          '#field' => $struct_field,
          '#struct_field_no_suggestion' => TRUE,
          '#ajax' => array(
            'callback' => 'structfield_add_term',
            'wrapper' => "struct_field-term-holder-$struct_field",
            'method' => 'replace',
          ),
        );
        if($wtype == 'struct_lookup_field_with_scones_suggest' || $wtype == 'struct_lookup_field_with_scones_tagging') {
          //add refresh button
          $form[$struct_field]['refresh'] = array(
            '#type' => 'button',
            '#value' => 'Recommend from SCONES',
            '#weight' => -11,
            '#field' => $struct_field,
            '#ajax' => array(
              'callback' => 'structfield_add_term',
              'wrapper' => "struct_field-term-holder-$struct_field",
              'method' => 'replace',
              'effect' => 'fade',
            ),
          );
        }
        if($wtype == 'struct_lookup_field_with_scones_tagging') {
          //add refresh from related content button
          $form[$struct_field]['refresh_related'] = array(
            '#type' => 'button',
            '#value' => 'Recommend from similar content',
            '#weight' => -11,
            '#field' => $struct_field,
            '#ajax' => array(
              'callback' => 'structfield_add_term',
              'wrapper' => "struct_field-term-holder-$struct_field",
              'method' => 'replace',
              'effect' => 'fade',
            ),
          );
          ctools_include('ajax');
          ctools_include('modal');
          ctools_modal_add_js();

          // Add browse link
          $query = array(
            'network' => variable_get('construct_OntologySettings_network', 'http://localhost/structwsf/ws'),
            'dataset' => $config['dataset'],
            'modal' => TRUE,
            'browse' => TRUE,
            'readonly' => TRUE,
          );
          $links = l('Browse & add', 'construct/ontology/view', array('query' => $query, 'attributes' => array('class' => 'ctools-use-modal')));

          $links .= ' | ';

          // Add suggestion link
          $custom_style = array(
            'suggestion-style' => array(
              'modalSize' => array(
                'type' => 'fixed',
                'width' => 700,
                'height' => 300,
              ),
              'animation' => 'fadeIn',
            ),
          );
          drupal_add_js($custom_style, 'setting');
          $query = array(
            'dataset' => $config['dataset'],
            'nid' => (arg(2) == 'edit') ? arg(1) : -1, 
          );
          $links .= l('Suggest terms', 'construct/suggest', array('query' => $query, 'attributes' => array('class' => 'ctools-modal-suggestion-style ctools-use-modal')));

          // Add the links to the widget
          $form[$struct_field]['ontologylinks'] = array(
            '#weight' => -12,
            '#markup' => $links,
          );
        }

        /*
         * This is a first attempt at tring to have fields kick off a
         * refresh when they are edited
        foreach($config['fields'] as $field_name) {
          $form[$field_name]['#ajax'] = array(
            'callback' => 'structfield_add_term',
            'wrapper' => "struct_field-term-holder-$struct_field",
            'method' => 'replace',
            'effect' => 'fade',
          );
          $form[$field_name]['#field'] = $struct_field;
        }
        */
      }
    }
  }
}

/**
 * Page callback for the suggestion form.
 */
function structfield_suggest_form_page() {
  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'title' => t('Suggestion Form'),
    'ajax' => TRUE,
  );
  $output = ctools_modal_form_wrapper('structfield_suggest_form', $form_state);
  if (!empty($form_state['executed'])) {
    ctools_modal_command_dismiss();
    ctools_modal_render(t('Submission received'), '<p>Thanks! You can close this pop up now.</p>') ;
  } else {
    print ajax_render($output);
  }
}

/**
 * Form builder for the suggestion form.
 */
function structfield_suggest_form($form, &$form_state) {
  global $user;
  $form['ontology'] = array(
    '#type' => 'hidden',
    '#value' => $_GET['dataset'],
  );
  $form['type'] = array(
    '#type' => 'hidden',
    '#value' => 'new term', // TODO Update this to support multiple types
  );
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('New term name'),
    '#required' => TRUE,
  );
  $form['change'] = array(
    '#type' => 'textarea',
    '#title' => t('Description of change'),
    '#description' => t('Example: "Add the new term ABC as a child term of XYZ"'),
    '#required' => TRUE,
  );
  $form['hostsite'] = array(
    '#type' => 'hidden',
    '#value' => variable_get('nhccn_construct_site_id', 'hin'),
  );
  $form['node'] = array(
    '#type' => 'hidden',
    '#value' => $_GET['nid'],
  );
  $form['username'] = array(
    '#type' => 'hidden',
    '#value' => $user->name,
  );
  $form['usermail'] = array(
    '#type' => 'hidden',
    '#value' => $user->mail,
  );
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submit callback for the suggestion form.
 */
function structfield_suggest_form_submit(&$form, &$form_state) {
  $suggestion = $form_state['values'];
  $headers = array('Content-Type' => 'application/x-www-form-urlencoded');
  global $base_url;
  $url = variable_get('nhccn_construct_suggest_url', $base_url . '/construct/suggest/submit');
  $response = drupal_http_request($url, array('method' => 'POST', 'headers' => $headers, 'data' => http_build_query(array('suggestion' => $suggestion))));
}

/**
 * structfield_add_term is a ajax callback 
 * It returns the field specified in the triggering element
 */
function structfield_add_term($form, &$form_state) {
  $element =  $form[$form_state['triggering_element']['#field']];
  return $element;

}

/**
 * validation function for the widget, Change the values to match those of 
 * a multi field text field
 */
function structfield_validate(&$element, &$form_state) {
  $values = array();
  foreach($element['#value'] as $uri => $title) {
    $values[] = array('value' =>$uri);
  }
  form_set_value($element, $values, $form_state);
      //form_set_error('field_text', "bob");
  return;

}



function _structfield_scones_request($content, $max_count = 64) {
  if(!preg_match("/[\S]/",$content)) {
    return array();
  }
  $network = variable_get("construct_OntologySettings_network","");
  //ask scones for releated uris
  $scones = new SconesQuery($network);
  $search = $scones->document($content)
    ->send(new DrupalQuerierExtension());
  if(!$search->isSuccessful()) {
    throw new Exception("{$search->error->name}: {$search->error->description}");
  }
  $search_xml = new SimpleXMLElement($search->getResultSet());
  $annotations_xml = $search_xml->xpath("AnnotationSet[@Name='Tagged Concepts']/Annotation[Feature[Name='type']/Value = 'class']/Feature[Name='URI']/Value");
  $related_uris = array();
  foreach($annotations_xml as $node) {
    $uri = (string) $node;
    //insert the uri if we have not seen it yet
    if(!isset($related_uris[$uri])) {
      $related_uris[$uri] = 0;
    }
    //increment the number of times we have seen the url
    $related_uris[$uri] ++;
  }
  arsort($related_uris);
  return array_slice($related_uris, 0 , $max_count-1);
}

function _structfield_uri_label_map($uris, $dataset, $cache_only= FALSE) {
  //get the cache or just an array
  $cache = structfield_uri_map_cache();
  
  $missing_uris = array_diff(array_keys($uris), array_keys($cache));
  if($missing_uris && !$cache_only) {
    $datasets = array_fill(0, sizeof($missing_uris), $dataset);
    $network = variable_get("construct_OntologySettings_network","");
    $crudRead = new CrudReadQuery($network);
    $crudRead->excludeLinksback()
      ->excludeReification()
      ->uri($missing_uris)           
      ->includeAttributes(array(Namespaces::$iron."prefLabel"))
      ->dataset($datasets)
      ->send(new DrupalQuerierExtension());
    if(isset($crudRead->error)) {
      throw new Exception("{$crudRead->error->name}: {$crudRead->error->description}");
    }
    $resultset = $crudRead->getResultset();
    if($crudRead->isSuccessful()) {
      foreach($missing_uris as $uri) {
        $subject = $resultset->getSubjectByUri($uri);
        if($subject) {
          $cache[$uri] = $subject->getPrefLabel();
        }
      }
    }
  }
  structfield_uri_map_cache($cache);
  $map =  array_intersect_key($cache, $uris);
  return $map;
}

/**
 * implements a page callback
 *
 * This is an autocomplete page 
 * @PARAM $session_key: the session key to use to look up field info
 * @PARAM $query: the text to match for autocomplete
 * @RETURN : JSON object with the uri as the key and teh prefLable as the value
 */
function structfield_text_autocomplete($session_key, $query) {
  $config = isset($_SESSION[$session_key]) ? $_SESSION[$session_key] : array();
  
  $config = $config + array(
    'filter' => Namespaces::$owl."Class",
    //'dataset' => variable_get("construct_SconesSettings_ControlledVocabularyDatasetUri", ""),
    'dataset' => FALSE,
    'suggest_count' => 15,
  );

  //search across all networks
  $suggestions = array();
  $networks = variable_get("WSF-Registry", array("http://localhost/ws/"));
  $query = $query ? "*$query**" : '*';

  foreach($networks as $network) {
    $search = new SearchQuery($network);
    $labelProperties = Namespaces::getLabelProperties();

    $search->excludeAggregates()
           ->sourceInterface(variable_get("structsearchapi_settings_interface_name", ''))    
           ->items(99) // Limit to 99 because 100+ just returns nothing.
           ->sort('preflabel', 'asc')
           ->attributeValuesFilters(Namespaces::$iron.'prefLabel', $query);
    if($config['filter']) {
      $search->typeFilter($config['filter']);
    }

    if($config['dataset']) {
      $search->datasetFilter($config['dataset']);
    }

    $search->send(new DrupalQuerierExtension());

    //extract the uri and prefLabel we want to return with jason
    $resultset = $search->getResultset();
    $i = $total = 0;
    if(isset($resultset)) { 
      foreach($resultset->getResultset() as $dataset => $results) { 
        $total += count($results);
        foreach($results as $uri => $result) {
          // This is where we limit the number of suggestions, rather than in the query itself.
          if ($i >= $config['suggest_count']) {
            continue;
          }
          $i++;
          $suggestions[$uri] = $result['prefLabel'];
        }
      }
      if ($total >= 99) {
        $total = '100+';
      }
      $suggestions[] = t('Showing 1-') . count($suggestions) . t(' of ') . $total . ' results.';
    }
  }
  //lets cache this label_map so that we do not have to query for it later
  structfield_uri_map_cache($suggestions);

  //incase we had result from multiple networks lets limit again to 15
  //$suggestions = array_splice($suggestions,0, $config['suggest_count']);
  
  drupal_json_output($suggestions);
}


/**
 * structfield_uri_map_cache is used for setting and getting the cached hash of url to label
 *
 * we use the static if we have it and if not go look for the cache
 *
 * @PARAM $map: a assoc array of new uri to label mappings
 * @RETURN: the current full map of uri to labels
 */
function structfield_uri_map_cache($map = NULL) {
  $static = &drupal_static(__FUNCTION__);
  if(!$static) {
    $static = ($call = cache_get(__FUNCTION__))? $call->data : array();
  }
  if(isset($map)) {
    $static = $map + $static;
    cache_set(__FUNCTION__, $static);
  }
  return $static;
}
