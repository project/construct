<?php

  $nbRecords = 0;
  $nbRecordsDone = 0;
  $nbStepsSize = 10;
  $source = "";
  $sourceWSF = "";
  $target = "";
  $targetWSF = "";
  
  if(isset($_GET["source"]))
  {
    $source = $_GET["source"];
    
    $source = explode(" :: ", $source);
    
    $sourceWSF = $source[1];
    $source = $source[0];
  }
  else
  {
    header("HTTP/1.1 500 Internal server error");   
    header('Content-type: text/plain');
    echo 'No source dataset specified';
    die;
  }
  
  if(isset($_GET["target"]))
  {
    $target = $_GET["target"];
    
    $target = explode(" :: ", $target);
    
    $targetWSF = $target[1];
    $target = $target[0];
  }
  else
  {
    header("HTTP/1.1 500 Internal server error");   
    header('Content-type: text/plain');
    echo 'No target dataset specified';
    die;
  }
  
  $token = md5(time());

  if(isset($_GET["token"]))
  {
    $token = $_GET["token"];
  }

  $userIP = "0.0.0.0";
  
  if(isset($_GET["userIP"]))
  {
    $userIP = $_GET["userIP"];
  }
  
  // Once we start the appendding process, we have to make sure that even if the server
  // loose the connection with the user the process will still finish.
  ignore_user_abort(true);

  // However, maybe there is an issue with the server handling that file tht lead to some kind of infinite or near
  // infinite loop; so we have to limit the execution time of this procedure to 45 mins.
  set_time_limit(2700);
  
  include_once("./../../framework/WebServiceQuerier.php");
  include_once("./../../framework/ProcessorXML.php");
  
  // First, check the number of records within the source dataset.
  $wsq = new WebServiceQuerier($sourceWSF . "browse/", "post", "text/xml",
    "attributes=all&types=all&datasets=" . urlencode($source) . "&items=0&inference=on&" .
    "include_aggregates=true&registered_ip=" . urlencode($userIP), 5);
  
  if ($wsq->getStatus() == 200)
  {
    $xml = new ProcessorXML();
    $xml->loadXML($wsq->getResultset());
    $test = $wsq->getResultset();

    $aggregates = $xml->getSubjectsByType("http://purl.org/ontology/aggregate#Aggregate");

    foreach ($aggregates as $aggregate)
    {
      $aggregetionType = $xml->getPredicatesByType($aggregate, "http://purl.org/ontology/aggregate#property");
      $aggregetionTypeObj = $xml->getObjects($aggregetionType->item(0));

      switch ($xml->getURI($aggregetionTypeObj->item(0)))
      {
        case "http://rdfs.org/ns/void#Dataset":
          $aggregateObject =
            $xml->getPredicatesByType($aggregate, "http://purl.org/ontology/aggregate#object");
          $aggregateObjectObj = $xml->getObjects($aggregateObject->item(0));
          
          if($xml->getURI($aggregateObjectObj->item(0)) == $source)
          {
            $aggregateCount = $xml->getPredicatesByType($aggregate, "http://purl.org/ontology/aggregate#count");
            $aggregateCountObj = $xml->getObjects($aggregateCount->item(0));
            
            $nbRecords = $xml->getContent($aggregateCountObj->item(0));
          }
        break;
      }
    }  
  }
  else
  {
    header("HTTP/1.1 500 Internal server error");   
    header('Content-type: text/plain');
    echo 'Can\'t get the total number of documents to append from the source dataset by using
    the browse web service endpoint.';
    
    die;
  }
  
                                                          
  
  // Check if the progress folder exists; create it if not.
//  $folder = dirname(realpath($_SERVER['SCRIPT_FILENAME']))."/progress/";

  while($nbRecordsDone < $nbRecords && $nbRecords > 0)
  {
    $percentDone = floor((($nbRecordsDone / $nbRecords) * 100));
    
    if($percentDone == 0)
    {
      $percentDone = "Initializing...";
    }
    
    $xml = "<?xml version=\"1.0\"?>  
            <DOCUMENT><PROGRESS>$percentDone</PROGRESS></DOCUMENT>";
            
    if(!file_exists("./progress/") || 
       !is_writable("./progress/") || 
       !file_put_contents("./progress/progress_$token.xml", $xml))
    {
      header("HTTP/1.1 500 Internal server error");   
      header('Content-type: text/plain');
      echo 'Can\'t create the progress file on the Drupal server. Please contact the
      system administrator to make sure that the web server has rights permissions
      to write files in the /.../progress/ folder.';
      die;
    } 
    
    // Read a slice of the source dataset
    $query =
      "select * where {{select distinct ?s where {?s a ?type.} limit $nbStepsSize offset " . $nbRecordsDone
      . "} ?s ?p ?o}";

    // Export records by using the SPARQL web service
    $wsq = new WebServiceQuerier($sourceWSF . "sparql/", "post", "application/rdf+n3",
      "query=" . urlencode(str_replace(array ("\n", "\r", "\t"), " ", $query)) .
      "&dataset=" . urlencode($source) .
      "&registered_ip=" . urlencode($userIP));
     
    if($wsq->getStatus() != 200 && strtolower($wsq->error->level) != "notice")
    {
      header("HTTP/1.1 500 Internal server error");   
      header('Content-type: text/plain');
      echo 'Can\'t get one of the data slice from the source dataset. '.$wsq->getStatusMessageDescription();
      die;
    }

    $rdfDocument = $wsq->getResultset();

    unset($wsq);    

    // Write the slice to the target dataset to the triple store only
    $wsq = new WebServiceQuerier($targetWSF . "crud/create/", "post", "text/xml",
      "document=" . urlencode($rdfDocument) .
      "&dataset=" . $target .
      "&registered_ip=" . urlencode($userIP) .
      "&mime=" . urlencode("application/rdf+n3") .
      "&mode=triplestore");

    if ($wsq->getStatus() != 200 && strtolower($wsq->error->level) != "notice")
    {
      header("HTTP/1.1 500 Internal server error");   
      header('Content-type: text/plain');
      echo 'Can\'t commit a slice to the target dataset. '. $wsq->getStatusMessage() . 
           $wsq->getStatusMessageDescription();
      die;
    }

    unset($wsq);
    
    // Read all the records that have been appended to the target dataset.
    
    // Get all URIs
    include_once("./../../framework/arc2/ARC2.php");        

    $parser = ARC2::getRDFParser();
    $parser->parse("", $rdfDocument);
    
//    $rdfxmlSerializer = ARC2::getRDFXMLSerializer();
    $rdfxmlSerializer = ARC2::getNTriplesSerializer();

    $resourceIndex = $parser->getSimpleIndex(0);

    $recordURIs = "";
    
    if(count($parser->getErrors()) <= 0 && $rdfxmlSerializer)
    {
      foreach($resourceIndex as $resource => $description)
      {
        // Only index what the CrudRead web service GEt query can handle per query (max URL lenght).
        if(strlen($recordURIs.urlencode($resource)) > 4000)
        {
          indexSolrRecords(trim($recordURIs, ";"), $targetWSF, $token);
          
          $recordURIs = urlencode($resource).";";       
        }
        else
        {
          $recordURIs .= urlencode($resource).";";
        }
      }
      
      indexSolrRecords(trim($recordURIs, ";"), $targetWSF, $token);
    }  
    else
    {
      header("HTTP/1.1 500 Internal server error");   
      header('Content-type: text/plain');
      
      $errors = "";
      
      foreach($parser->getErrors() as $error)
      {
        $errors .= $error.".";
      }
      
      echo "Can't parse dataset slice. Please contact the system administrator to fix this problem.\n
           Errors: $errors\n\n";
      die;
    }  
    
    unset($parser);
    
    $nbRecordsDone += $nbStepsSize;
  }

  $xml = "<?xml version=\"1.0\"?>  
          <DOCUMENT><PROGRESS>100</PROGRESS></DOCUMENT>";

  file_put_contents("./progress/progress_$token.xml", $xml);

  header("HTTP/1.1 200 OK");   
  header('Content-type: text/plain');
  
  
  // remove file at the end of the process...


  
  
  
  function indexSolrRecords($recordURIs, $targetWSF)
  {
    global $progressDone, $nbStepsSize, $target, $userIP;
    

    $wsq = new WebServiceQuerier($targetWSF . "crud/read/", "get", "application/rdf+n3",
      "uri=" . $recordURIs . "&registered_ip=".urlencode($userIP).
      "&include_reification=true&include_linksback=false");
      
    $rdfDocument = "";
    
    if ($wsq->getStatus() != 200 && strtolower($wsq->error->level) != "notice")
    {
      header("HTTP/1.1 500 Internal server error");   
      header('Content-type: text/plain');
      echo 'Can\'t commit a slice to the target dataset. '. $wsq->getStatusMessage() . 
           $wsq->getStatusMessageDescription();

      die;
    }
    else
    {
      $rdfDocument = $wsq->getResultset();    
    }  

    unset($wsq);
       
    // Finally write all the records to Solr. That way we make sure that if some of the
    // appended records where already in the target dataset, that we will re-write
    // the merged result of the records, and not simply what comes from the source dataset
    // to the Solr index.
    //
    // We have to do this for the only reason that we can't partially update Solr documents
    // at this time (the feature is not supported).

    // Write the slice to the target dataset to Solr only
    $wsq = new WebServiceQuerier($targetWSF . "crud/create/", "post", "text/xml",
      "document=" . urlencode($rdfDocument) .
      "&dataset=" . $target .
      "&registered_ip=" . urlencode($userIP) .
      "&mime=" . urlencode("application/rdf+n3") .
      "&mode=searchindex");

    if ($wsq->getStatus() != 200 && strtolower($wsq->error->level) != "notice")
    {
      header("HTTP/1.1 500 Internal server error");   
      header('Content-type: text/plain');
      echo 'Can\'t commit a slice to the target dataset. '. $wsq->getStatusMessage() . 
           $wsq->getStatusMessageDescription()."\n\n";
           
      file_put_contents("/tmp/rdfError.rdf", $rdfDocument);
      die;
    }

    unset($wsq);    
  }
?>
